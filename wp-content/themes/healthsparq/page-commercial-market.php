<?php
/*
Template Name: Page Commercial Market
*/
get_header(); ?>

<div class="container">

	<?php
	$header_img = get_field( 'header_image' );
	$uuid       = uniqid( 'img' );
	if ( $header_img )
	{
		echo Picture::create( 'style', $header_img, [
			'selector' => '#' . $uuid,
		] );
	}
	$siblings = wp_list_pages( [
		'child_of' => $post->post_parent,
		'depth'    => 1,
		'echo'     => false,
		'title_li' => null,
	] );
	?>

	<header class="landing-header commercial-market-header" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<div class="copy">
			<label class="type"><?php the_title(); ?></label>
			<div class="header-content">
				<?php the_field( 'header_content' ); ?>
			</div>
		</div>
	</header>

	<section class="main solution">
		<?php the_content(); ?>
	</section>

	<?php $flex_blocks = get_field( 'flex_modules' ); ?>
	<?php if ( $flex_blocks ): ?>
		<?php
		$i              = 0;
		$layouts_module = [];
		$totalFlexCount = count( $flex_blocks ) ?? 0;
		$smallFexsIDs   = [];
		for ( $key = 0; $key < $totalFlexCount; )
		{
			if ( $flex_blocks[ $key ]['acf_fc_layout'] == 'small_video' || $flex_blocks[ $key ]['acf_fc_layout'] == 'small_post' || $flex_blocks[ $key ]['acf_fc_layout'] == 'small_link' || $flex_blocks[ $key ]['acf_fc_layout'] == 'small_wysiwyg' || $flex_blocks[ $key ] == 'wysiwyg_header' )
			{
				array_push( $smallFexsIDs, $key );
			}
			$key ++;
		}
		$firstSmallFlexID = $smallFexsIDs[0];

		foreach ( $flex_blocks as $block )
		{
			$type_module = 'flex';
			include( 'flex-layouts/' . $block['acf_fc_layout'] . '.php' );
			$layouts_module[] = $block['acf_fc_layout'];
			$i ++;
		}

		?>
	<?php endif; ?>

	<?php $blocks = get_field( 'callouts' ); ?>
	<?php if ( $blocks ) { ?>
		<?php if ( get_field( 'section_heading' ) ): ?>
			<h2 class="callouts text-center"><?php the_field( 'section_heading' ); ?></h2>
		<?php endif; ?>
		<?php
		$i               = 0;
		$layouts         = [];
		$totalBlockCount = count( $blocks ) ?? 0;
		// $islastBlock     = $totalBlockCount == $i;
		$smallBocksIDs   = [];
		for ( $key = 0; $key < $totalBlockCount; )
		{
			if ( $blocks[ $key ]['acf_fc_layout'] == 'small_video' || $blocks[ $key ]['acf_fc_layout'] == 'small_post' || $blocks[ $key ]['acf_fc_layout'] == 'small_link' || $blocks[ $key ] == 'small_wysiwyg' || $blocks[ $key ] == 'wysiwyg_header' )
			{
				array_push( $smallBocksIDs, $key );
			}
			$key ++;
		}
		$firstSmallFlexID = $smallBocksIDs[0];

		foreach ( $blocks as $block )
		{
			$type_module = 'callout';
			include( 'flex-layouts/' . $block['acf_fc_layout'] . '.php' );
			$layouts[] = $block['acf_fc_layout'];
			$i ++;
		}
		?>

	<?php } ?>


</div>

<?php get_footer(); ?>

<script src="<?php echo get_template_directory_uri() ?>/library/js/vendor/jquery.viewportchecker.min.js"></script>
<script>
	$( document ).ready( function () {
		$( 'li.solution, .section-capabilities .btn, .section-why-health-sparq .btn' ).viewportChecker( {
			classToAdd: 'visible animated fadeInUp',
			offset: 100
		} );
	} );
</script>
</body>
</html>
