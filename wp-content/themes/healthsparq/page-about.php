<?php
/*
Template Name: About
*/
get_header(); ?>

<div class="container about">

	<?php include('inc/landing-header.php'); ?>

	<section class="main">
		<?php the_content(); ?>
	</section>

	<?php $peeps = get_field('team_slider'); ?>

	<section id="team" class="flexslider team-slider">
	  <ul class="slides">
	    <?php
	    	foreach ($peeps as $post) {
	    		setup_postdata($post);
	    		include('inc/team-slide.php');
	    	}
	    ?>
	  </ul>
		<div class="nav-arrows">
			<a class="flex-prev"><img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-left.svg" alt="Previous" /></a>
			<a class="flex-next"><img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-right.svg" alt="Next" /></a>
		</div>
	</section>

	<?php wp_reset_query(); ?>

	<?php $leaders = get_field('leadership'); ?>

	<section id="leadership" class="leadership">
		<h2>Our Leadership</h2>
		<?php foreach ($leaders as $post) {
			setup_postdata($post);
			include('inc/leader.php');
		} ?>
		<div class="clear"></div>
	</section>

	<?php wp_reset_postdata(); ?>

	<section class="main">
		<?php echo the_field('post_leadership_content'); ?>
	</section>

</div><!-- /.container -->

<?php $blocks = get_field('callouts'); ?>
<?php if ($blocks) { ?>
<section class="callouts">
  <h2><?php the_field('section_heading'); ?></h2>
  <?php
    $i = 0;
    $layouts = array();
    foreach ($blocks as $block) {
      include('flex-layouts/'.$block['acf_fc_layout'].'.php');
      $layouts[] = $block['acf_fc_layout'];
      $i++;
    }
    // print_r($layouts);
  ?>
  <div class="clear"></div>
  <?php if ( in_array('video_header', $layouts)  || in_array('small_video', $layouts) ) { ?>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/library/js/video.js"></script>
  <?php } ?>
</section>
<?php } ?>



<?php get_footer(); ?>

</body>
</html>
