<?php get_header(); ?>

<div class="container">

	<?php

		$header_img = get_field('header_image','option');
		$uuid = uniqid('img');
		if ($header_img) {
			echo Picture::create( 'style', $header_img, array(
			    'selector' => '#'.$uuid
			) );
		}

	?>

	<header class="landing-header" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<div class="copy">
			<label class="type">Press &amp; News</label>
			<?php the_field('header_content','option'); ?>
		</div>
	</header>

	<section class="index press-index layoutPending">
		<div class="block-sizer"></div>
		<div class="gutter-sizer"></div>
		<?php while (have_posts()) : the_post(); ?>
			<?php include('inc/press-block.php'); ?>
		<?php endwhile; ?>
		<div class="clear"></div>
	</section>

	<?php include('inc/pagination.php'); ?>

	<section class="press-contact">
		<?php if( have_rows('press_contact','option') ): ?>
			<h6>Press Contact</h6>
			<div class="grid">

			<?php while( have_rows('press_contact','option') ): the_row();

				// vars
				$fname = get_sub_field('first_name');
				$lname = get_sub_field('last_name');
				$phone = get_sub_field('phone');
				$email = get_sub_field('email');
				$button = get_sub_field('button_text');

				?>

				<div class="col-1-3">
					<h5><?php echo $fname . ' ' . $lname; ?></h5>
					<p><?php echo $phone ?></p>
					<a class="btn tertiary small" href="mailto:<?php echo $email; ?>"><?php if( $button ) { echo $button; } else { echo 'Email ' . $fname; }?></a>
				</div>

			<?php endwhile; ?>

		</div>

		<?php endif; ?>
	</section>

</div>

<?php get_footer(); ?>

</body>
</html>
