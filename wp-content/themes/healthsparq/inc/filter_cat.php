<?php

	if ( $post->post_type == 'post' ) {
		$include = array(39,40,41,42,43);
		$exclude = array(1);
		$view_all = 'View All Posts';
	} elseif ( $post->post_type == 'press' ) {
		$include = array(29,30);
		$exclude = array(1);
		$view_all = 'View All';
	} else {
		$view_all = 'View All';
		$exclude = array(1);
		$include = array();
	}
	$filters = wp_list_categories(array(
		// 'child_of' => $post->post_parent,
		'show_count' => 1,
		'show_option_all' => $view_all,
		'echo' => false,
		'title_li' => null,
		'exclude' => $exclude,
		'include' => $include
	));
?>


<?php if ($filters) { ?>
	<div class="dropdown-container">
    <div class="dropdown closed">
			<div class="title"><?php echo $view_all; ?></div>
			<div class="dropdown-menu" style="height:0;">
        <ul>
          <?php echo $filters; ?>
        </ul>
			</div>
    </div>
	</div>
<?php } ?>
