
<?php
$cutout = get_field('cutout');
$pie = get_field('cutout_with_pie');
$cutout_src = wp_get_attachment_image_src($cutout,'medium_large');
$pie_src = wp_get_attachment_image_src($pie,'medium_large');
// $cutout_settings = array(
// 	'attributes' => array(
// 		'class' => 'no-pie',
// 		// 'sizes' => '(min-width: 500px) 1024px, 300px'
// 		'sizes' => array('medium', 'medium_large', 'large'),
// 		'media_queries' => array(
// 			'medium_large' => 'min-width: 991px',
// 			'large' => 'min-width: 1024px'
// 		)
// 	)
// );
// $pie_settings = array(
// 	'attributes' => array(
// 		'class' => 'pie',
// 		// 'sizes' => '(min-width: 500px) 1024px, 300px'
// 		'sizes' => array('thumbnail', 'medium', 'medium_large'),
// 		'media_queries' => array(
// 			'medium' => 'min-width: 500px',
// 			'medium_large' => 'min-width: 991px'
// 		)
// 	)
// );


?>

<li class="team-slide" style="background-color:<?php the_field('background_color'); ?>;">
	<div class="image">
		<img class="no-pie " src="<?php echo $cutout_src[0] ?>" alt="<?php the_title(); ?>" />
		<img class="pie " src="<?php echo $pie_src[0] ?>" alt="<?php the_title(); ?>" />
		<!-- <img class="placeholder" src="<?php// echo get_stylesheet_directory_uri(); ?>/library/img/icons/avatar.svg" /> -->
	</div>
	<div class="copy">
		<h4>Meet <?php the_title(); ?></h4>
		<h5><?php the_field('title'); ?></h5>
		<?php the_excerpt(); ?>
	</div>
</li>
