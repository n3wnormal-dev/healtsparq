<?php
$showPost = false;

$img  = get_post_thumbnail_id();
$uuid = uniqid( 'img' );
if ( $img )
{
	echo Picture::create( 'style', $img, [
		'selector' => '#' . $uuid,
	] );
}

$objDateTime = strtotime( date( 'M j' ) );

if ( get_field( 'event_date_type' ) == 'range' )
{
	if ( strtotime( get_field( 'end_date' ) ) >= $objDateTime )
	{
		$date_string = date( 'M j', strtotime( get_field( 'start_date' ) ) ) . ' - ' . date( 'M j', strtotime( get_field( 'end_date' ) ) );
		$showPost    = true;
	}

}
else
{
	$dates      = get_field( 'event_dates' );
	$date_array = [];
	foreach ( $dates as $date )
	{
		if ( strtotime( $date['date'] ) >= $objDateTime )
		{
			$date_array[] = date( 'M j', strtotime( $date['date'] ) );
			$showPost     = true;
		}
	}
	$date_string = implode( ', ', $date_array );
}

if ( ! get_field( 'no_link' ) && $showPost )
{

	$link   = get_permalink();
	$format = get_post_format();
	if ( $format == 'link' )
	{
		$link = get_field( 'external_url' );
	} ?>

	<a <?php if ( $format == 'link' ) {
		echo 'target="_blank"';
	} ?>class="event <?php if ( $i === 0 && $count != 2 ) {
		echo 'header';
	} ?>" href="<?php echo $link; ?>" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<label class="type dates"><?php echo $date_string; ?> &nbsp;|&nbsp; <?php the_field( 'location' ); ?></label>
		<h4><?php the_title(); ?></h4>
	</a>

<?php } else if ( $showPost ) { ?>

	<div class="event <?php if ( $i === 0 && $count != 2 ) {
		echo 'header';
	} ?>" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<label class="type dates"><?php echo $date_string; ?> &nbsp;|&nbsp; <?php the_field( 'location' ); ?></label>
		<h4><?php the_title(); ?></h4>
	</div>

<?php } ?>
