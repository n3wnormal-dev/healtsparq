<?php

$header_img = get_field('header_image');
$uuid = uniqid('img');
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
}

?>

<header class="landing-header" id="<?php echo $uuid; ?>">
	<div class="overlay"></div>
	<div class="copy">
		<label class="type"><?php the_title(); ?></label>
		<h1><?php the_field('header_content'); ?></h1>
	</div>
</header>
