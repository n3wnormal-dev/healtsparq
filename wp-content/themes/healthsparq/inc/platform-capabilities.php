
<?php if ( have_rows( 'capabilities' ) ): ?>
	<div class="section-capabilities">
		<h3><?= get_field('capabilities_title'); ?></h3>
		<ul>
			<?php while ( have_rows( 'capabilities' ) ) : the_row(); ?>
				<li class="capability">
						<span class="wrapper-image">
								  <img src="<?= get_sub_field( 'image' )['url']; ?>" alt="">
						</span>
					<span class="meta">
							<span class="title"><?= get_sub_field( 'title' ); ?></span>
							<span class="description"><?= get_sub_field( 'description' ); ?></span>
						</span>
				</li>
			<?php endwhile; ?>
		</ul>
		<?php if (get_field('cta_button') && get_field('platform_section_show_demo_button')): ?>
			<p style="text-align: center;"><a class="btn hidden" href="<?= get_field('cta_button')['url'] ?>"><?= get_field('cta_button')['title'] ?></a></p>
		<?php endif; ?>
	</div>
<?php endif; ?>
