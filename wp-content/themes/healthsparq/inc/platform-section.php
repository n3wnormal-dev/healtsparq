<?php if ( have_rows( 'features' ) ): ?>
	<div class="platform-section">
		<h2><?php echo get_field( 'features_title' ); ?></h2>
		<div class="section-features">
			<ul>
				<?php while ( have_rows( 'features' ) ) : the_row(); ?>
					<li>
						<a href="<?= get_sub_field( 'link' )['url'] ?>">
							<span class="wrapper-image">
							  <img src="<?= get_sub_field( 'icon' )['url']; ?>" alt="">
							</span>
							<span class="description"><?= get_sub_field( 'description' ); ?></span>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>
