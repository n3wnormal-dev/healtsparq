<?php

$format = get_post_format();

if ($format == 'link') {
	$link = get_field('external_url');
	$the_url = parse_url($link);
	$parsed_url = preg_replace('/^www\./', '', $the_url['host']);


} else {
	$link = get_permalink();
}

?>

<a <?php if ($format == 'link') { echo 'target="_blank"'; } ?>class="block <?php if ($format == 'link') { echo 'external'; } ?>" href="<?php echo $link; ?>">
	<h3><?php the_title(); ?></h3>
	<div class="date"><?php echo get_the_date(); ?></div>
	<div class="snippet"><?php echo wp_trim_words(get_the_excerpt(), 60, '...'); ?></div>
	<?php if ($format == 'link') { ?>
		<div class="link">Go to <?php echo $parsed_url; ?></div>
	<?php } else { ?>
		<div class="link">read more</div>
	<?php } ?>
</a>
