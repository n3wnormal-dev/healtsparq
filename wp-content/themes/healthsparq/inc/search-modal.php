<div data-remodal-id="search" class="remodal modal search">
	<a data-remodal-action="close" class="remodal-close"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-x.svg" alt="&times;" /></a>
	<div class="container">
    <?php get_search_form(); ?>
	</div>
</div>
