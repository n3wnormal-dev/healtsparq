<?php $blocks = get_field('callouts'); ?>
<?php if ($blocks) { ?>
<section class="callouts">
  <h2><?php the_field('section_heading'); ?></h2>
  <?php
    $i = 0;
    $layouts = array();
    foreach ($blocks as $block) {
      include('flex-layouts/'.$block['acf_fc_layout'].'.php');
      $layouts[] = $block['acf_fc_layout'];
      $i++;
    }
    // print_r($layouts);
  ?>
  <div class="clear"></div>
  <?php if ( in_array('video_header', $layouts)  || in_array('small_video', $layouts) ) { ?>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/video.js"></script>
  <?php } ?>
</section>
<?php } ?>
