<div class="notification-bar">
	<div class="notification-bar__inner container">
		<span><?= get_field( 'cta_text', 'options'); ?> <a href="<?= get_field( 'cta_link', 'options')['url']; ?>" <?php if ( get_field( 'cta_link', 'options')['target'] == "_blank"): ?> target="_blank" <?php endif; ?> class="hide-bar">Learn more</a></span>
		<span class="close">+</span>
	</div>

	<style>
		body.notification-show header {
			top: 40px;
		}

		body.notification-show .notification-bar {
			display: block;
		}

		.notification-bar {
			display: none;
			background: #29b5a8;
			color: #fff;
			font-family: ProximaNova-Regular, proxima-nova, "Proxima Nova", "Helvetica Neue", Lato, "Open Sans", Roboto, "Franklin Gothic Medium", Helvetica, Arial, sans-serif;
			font-size: 16px;
			text-align: center;
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			z-index: 2000;
			height: 40px;
		}

		.notification-bar__inner {
			display: flex;
			align-items: center;
			justify-content: center;
			height: 100%;
			position: relative;
		}

		.notification-bar__inner.container {
			background-color: transparent;
			padding-right: 60px;
			padding-left: 60px;
		}

		.notification-bar__inner a {
			color: #fff;
			text-decoration: underline;
		}

		.notification-bar__inner a:hover {
			text-decoration: none;
		}

		.notification-bar__inner .close {
			position: absolute;
			top: 50%;
			left: auto;
			right: 35px;
			transform: translate(-50%, -50%) rotate(45deg);
			cursor: pointer;
			font-size: 20px;
		}

		@media all and (max-width: 768px) {
			.notification-bar__inner .close {
				display: none;
			}
			.notification-bar__inner a {
				display: block;
			}
			.notification-bar__inner.container {
				padding-left: 20px;
				padding-right: 20px;
			}
			.notification-bar {
				height: 52px;
				padding-top: 10px;
				padding-bottom: 10px;
			}
			body.notification-show header {
				top: 52px;
			}
		}
	</style>
</div>
<script>

	document.addEventListener( "DOMContentLoaded", function ( event ) {

		function setCookie ( name ) {
			document.cookie = name + '=hide';
		}

		function delCookie ( name ) {
			document.cookie = name + '=';
		}

		function getCookie ( cname ) {
			var name = cname + "=";
			var ca = document.cookie.split( ';' );
			for ( var i = 0; i < ca.length; i++ ) {
				var c = ca[ i ];
				while ( c.charAt( 0 ) == ' ' ) {
					c = c.substring( 1 );
				}
				if ( c.indexOf( name ) == 0 ) {
					return c.substring( name.length, c.length );
				}
			}
			return "";
		}

		function showNotifyBar () {
			jQuery( 'body' ).addClass( 'notification-show' );
		}

		function checkCookie () {
			var notifyBar = getCookie( "notify_bar" );

			if ( notifyBar !== 'hide' ) {
				showNotifyBar();
			} else {
				jQuery('.notification-bar').remove();
			}
		}

		checkCookie();
		//delCookie("notify_bar");

		jQuery( 'body' ).on( 'click', '.notification-bar .close, .notification-bar .hide-bar', function () {
			setCookie( 'notify_bar' );
			jQuery( 'body' ).removeClass( 'notification-show' );
			jQuery('.notification-bar').remove();
		} )
	} );
</script>

