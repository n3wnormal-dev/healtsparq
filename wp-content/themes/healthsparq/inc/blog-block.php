<?php

$img = get_post_thumbnail_id();
$uuid = uniqid('img');
if ($img) {
	echo Picture::create( 'style', $img, array(
	    'selector' => '#'.$uuid
	) );
}

?>

<a class="block" href="<?php the_permalink(); ?>">
	<?php if ($img) { ?>
		<div class="image" id="<?php echo $uuid; ?>"></div>
	<?php } else { ?>
		<div class="image default"></div>
	<?php } ?>
	<h3><?php the_title(); ?></h3>
	<div class="snippet"><?php echo blog_excerpt(); ?></div>
	<div class="author">
		by <?php the_author(); ?> &nbsp;|&nbsp;
		<?php echo get_the_date(); ?>
	</div>
</a>
