
<?php

$img = get_post_thumbnail_id();
$uuid = uniqid('img');
if ($header_img) {
	echo Picture::create( 'style', $img, array(
	    'selector' => '#'.$uuid
	) );
}

?>

<section class="result">
  <?php if ($img) { ?>
		<a href="<?php the_permalink(); ?>"><div class="image" id="<?php echo $uuid; ?>"></div></a>
	<?php } ?>
	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<div class="snippet"><?php echo get_the_excerpt(); ?></div>
	<!-- <div class="author">
		by <?php //the_author(); ?> &nbsp;|&nbsp;
		<?php //echo get_the_date(); ?>
	</div> -->
	<div class="link"><a href="<?php the_permalink(); ?>">Read More</a></div>
</section>
