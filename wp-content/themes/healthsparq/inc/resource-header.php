<?php
// Get current $category
if (is_archive( get_queried_object_id() )) { $current = ' current-cat'; }
$header_img = get_field('header_image',77);
$uuid = uniqid('img');
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
}
$args = $args = array(
    'parent' => 0,
    'hide_empty' => true,
    'exclude' => 45,
);
$terms = get_terms('resource_type', $args);
if (is_archive()) {
	$the_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$current = ' current_page_item';
}

?>

<header class="landing-header resources-header" id="<?php echo $uuid; ?>">
	<div class="overlay"></div>
	<div class="copy">
		<label class="type"><?php echo get_the_title(77); ?></label>
		<h1><?php echo get_field('header_content',77); ?></h1>
	</div>
	<nav class="subnav resource-type-links" data-current="<?php echo $the_term->slug; ?>">
		<a href="/resources" class="<?php if (is_post_type_archive()) { echo 'current_page_item'; } ?>">
			<div class="image" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/img/icons/all.svg);"></div>
			<span>All</span>
		</a>
		<?php	foreach ($terms as $term) { ?>
			<a href="<?php echo get_term_link($term,'resources'); ?>"  class="<?php echo $term->slug; if ($the_term->slug == $term->slug ) { echo $current; } ?>">
				<div class="image" style="background-image:url(<?php echo the_field('key_image',$term); ?>);"></div>
				<span><?php echo $term->name; ?></span>
			</a>
		<?php } ?>

		<div class="dropdown-container mobile">
	    <div class="dropdown closed">
				<div class="title">

					<?php if (is_post_type_archive()) { ?>
						<div class="image all" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/img/icons/all.svg);"></div>
						<label>All Resources</label>
					<?php } else { ?>
						<div class="image <?php echo $the_term->slug; ?>" style="background-image:url(<?php echo the_field('key_image',$the_term); ?>);"></div>
						<label><?php echo $the_term->name; ?></label>
					<?php } ?>
				</div>
				<div class="dropdown-menu" style="height:0;">
	        <ul>
						<li class="<?php if (is_post_type_archive()) { echo 'current_page_item'; } ?>">
							<a href="/resources">
								<div class="image" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/img/icons/all.svg);"></div>
								<label>All</label>
							</a>
						</li>
						<?php	foreach ($terms as $term) { ?>
							<li class="<?php if ($the_term->slug === $term->slug) { echo 'current_page_item'; } ?>">
								<a href="<?php echo get_term_link($term,'resources'); ?>"  class="<?php echo $term->slug?>">
									<div class="image" style="background-image:url(<?php echo the_field('key_image',$term); ?>);"></div>
									<label><?php echo $term->name; ?></label>
								</a>
							</li>
						<?php } ?>
	        </ul>
				</div>
	    </div>
		</div>
	</nav>
</header>
