	<?php if ( have_rows( 'solutions' ) ): ?>
	<section class="guidance-solutions main">
		<h2><?= get_field('guidance_solutions_title'); ?></h2>
		<div class="section-solutions">
			<ul>
				<?php while ( have_rows( 'solutions' ) ) : the_row(); ?>
					<li class="solution invisible">
						<span class="description"><?= get_sub_field( 'description' ); ?></span>
						<span class="wrapper-image">
								  <img src="<?= get_sub_field( 'icon' )['url']; ?>" alt="">
							</span>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
	</section>
<?php endif; ?>
