<?php
$name = get_the_title();
 ?>
<a class="leader" data-remodal-target="<?php echo sanitize_title_with_dashes($name); ?>" style="background-color:<?php the_field('background_color'); ?>" data-href="<?php echo get_permalink($post->ID); ?>">
	<?php
		$img = get_post_thumbnail_id($post->ID);
		$uuid = uniqid('img');
		if ($img) {
			echo Picture::create( 'style', $img, array(
			    'selector' => '.'.$uuid,
          'sizes' => array('medium', 'medium_large', 'large')
			) );
		}
	?>
	<div class="image">
		<div class="background grayscale <?php echo $uuid; ?>"></div>
		<div class="overlay" style="background-color:<?php the_field('background_color'); ?>"></div>
	</div>
	<div class="copy">
		<h4><?php the_title(); ?></h4>
		<h5><?php the_field('title'); ?></h5>
		<p><?php echo leader_excerpt(); ?></p>
	</div>
</a>

<div data-remodal-id="<?php echo sanitize_title_with_dashes($name); ?>" class=" remdal modal leader" style="background-color:<?php the_field('background_color'); ?>">
	<a data-remodal-action="close" class="remodal-close"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-x.svg" alt="&times;" /></a>
	<div class="image">
		<div class="background grayscale <?php echo $uuid; ?>" id=""></div>
		<div class="overlay" style="background-color:<?php the_field('background_color'); ?>"></div>
	</div>
	<div class="copy">
		<h4><?php the_title(); ?></h4>
		<h5><?php the_field('title'); ?></h5>
		<div class="content">
			<?php the_content(); ?>
		</div>
	</div>
</div>
