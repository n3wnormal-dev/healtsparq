<?php

$terms = get_the_terms($post->ID, 'resource_type');
$term = $terms[0];

?>
<?php //if ($term->slug != 'private') { ?>
	<a class="block <?php echo $term->slug; ?>" href="<?php the_permalink(); ?>" style="background-image:url(<?php the_field('secondary_image',$term); ?>);">
		<h3><?php the_title(); ?></h3>
		<p><?php echo resource_excerpt(); ?></p>
		<div class="link"><?php the_field('cta_term',$term); ?></div>
	</a>

<?php //} ?>
