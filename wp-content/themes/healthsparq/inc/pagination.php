<footer>
  <?php the_posts_pagination( array(
    'mid_size' => 5,
    'prev_text' => __( '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/icons/ico-page-prev.svg" />' ),
    'next_text' => __( '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/icons/ico-page-next.svg" />' ),
  ) );  ?>
</footer>