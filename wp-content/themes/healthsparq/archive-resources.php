<?php get_header(); ?>

<div class="container">

	<?php include('inc/resource-header.php'); ?>

	<section class="index resource-index layoutPending">
		<div class="block-sizer"></div>
		<div class="gutter-sizer"></div>
		<?php while (have_posts()) : the_post(); ?>
			<?php include('inc/resource-block.php'); ?>
		<?php endwhile; ?>
		<div class="clear"></div>
	</section>
	<?php include('inc/pagination.php'); ?>
	<?php wp_reset_query(); ?>
</div>

<?php get_footer(); ?>

</body>
</html>
