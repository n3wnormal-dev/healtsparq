<?php // wp_redirect( home_url() ); exit; ?>

<?php get_header(); ?>

<main>
    <label class="type left neutral">error 404</label>
    <h1 class="secondary">oops, something <br/>went wrong.</h1>
    <p>Let's make it right.  Try one of these links:</p>
    <?php wp_nav_menu(array('theme_location' => 'main-nav')); ?>
</main>

<?php get_footer(); ?>

</body>
</html>
