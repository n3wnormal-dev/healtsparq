<footer>

	<?php if (!is_404() && !is_page('demo') && !is_search() && !is_page('contact')) { ?>
	<div class="super-footer">
		<div class="container">
			<h3><?php the_field('super_footer_heading','options'); ?></h3>
			<a class="btn" href="<?php the_field('super_footer_button_link','options'); ?>">
				<?php the_field('super_footer_button_text','options'); ?>
			</a>
			<?php the_field('super_footer_message','options'); ?>
		</div>
	</div>
	<?php } ?>

	<div class="main-footer desktop">
		<div class="container">
			<?php if (!is_404()) { ?>
			<div class="grid">
				<div class="col-2-3">
					<div class="col-">
						<h5><?php the_field('footer_menu_1_heading','option'); ?></h5>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-1' ) ); ?>
					</div>
					<div class="col-">
						<h5><?php the_field('footer_menu_2_heading','option'); ?></h5>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-2' ) ); ?>
					</div>
					<div class="col-">
						<h5><?php the_field('footer_menu_3_heading','option'); ?></h5>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-3' ) ); ?>
					</div>
				</div>

				<div class="col-1-3">
					<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/hsq_logo_cropped.png" alt="HealthSparq" />
					<div class="socials">
				    <?php
				    	$socials = get_field('social_links','options');
				    	foreach ($socials as $social) { ?>
				    		<a href="<?php echo $social['url']; ?>" target="_blank">
				    			<i class="<?php echo $social['channel']['value']; ?>">
				    				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-social-<?php echo $social['channel']['value']; ?>.svg" alt="<?php echo $social['channel']['label']; ?>" />
				    			</i>
				    		</a>
				    	<?php }
				    ?>
					</div>
				</div>
			</div>
			<?php } ?>
			<nav class="utility-nav" id="utility-nav">
				&copy; <?php echo date('Y'); ?> HealthSparq <?php wp_nav_menu( array( 'theme_location' => 'utility-menu', 'container' => false ) ); ?>
			</nav>
		</div>
	</div>


	<div class="mobile-footer">
		<div class="container">
			<?php if (!is_404()) { ?>
				<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/hsq_logo_cropped.png" alt="HealthSparq" />
				<section>
					<div class="accordion"><?php the_field('footer_menu_1_heading','option'); ?> <img class="caret" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-carrot-turq.svg" alt="HealthSparq" /></div>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-1' ) ); ?>
					<div class="accordion"><?php the_field('footer_menu_2_heading','option'); ?> <img class="caret" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-carrot-turq.svg" alt="HealthSparq" /></div>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-2' ) ); ?>
					<div class="accordion"><?php the_field('footer_menu_3_heading','option'); ?> <img class="caret" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-carrot-turq.svg" alt="HealthSparq" /></div>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-3' ) ); ?>
				</section>
				<section>
					<!-- <input type="email" placeholder="Enter email for newsletter" /> -->
					<div class="socials">
				    <?php
				    	$socials = get_field('social_links','options');
				    	foreach ($socials as $social) { ?>
				    		<a href="<?php echo $social['url']; ?>" target="_blank">
				    			<i class="<?php echo $social['channel']['value']; ?>">
				    				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-social-<?php echo $social['channel']['value']; ?>.svg" alt="<?php echo $social['channel']['label']; ?>" />
				    			</i>
				    		</a>
				    	<?php }
				    ?>
					</div>
				</section>
			<?php } ?>
			<nav class="utility-nav" id="utility-nav">
				&copy; <?php echo date('Y'); ?> HealthSparq <?php wp_nav_menu( array( 'theme_location' => 'utility-menu', 'container' => false ) ); ?>
			</nav>
		</div>
	</div>

	<!-- </div> -->

</footer>

<?php if (!is_404()) {
	include('inc/search-modal.php');
} ?>

<script type="text/javascript">
(function() {
	var didInit = false;
	function initMunchkin() {
		if(didInit === false) {
			didInit = true;
			Munchkin.init('130-SXO-349');
		}
	}
	var s = document.createElement('script');
	s.type = 'text/javascript';
	s.async = true;
	s.src = '//munchkin.marketo.net/munchkin.js';
	s.onreadystatechange = function() {
		if (this.readyState == 'complete' || this.readyState == 'loaded') {
			initMunchkin();
		}
	};
	s.onload = initMunchkin;
	document.getElementsByTagName('head')[0].appendChild(s);
})();
</script>


<link rel="stylesheet" href="https://use.typekit.net/jdc7vru.css">
<?php wp_footer(); ?>
