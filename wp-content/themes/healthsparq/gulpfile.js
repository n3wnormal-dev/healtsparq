'use strict';

// Require Dependencies
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require("autoprefixer");
const cleanCSS = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const gcmq = require('gulp-group-css-media-queries');
const uglifyjs = require('gulp-uglify-es').default;
const concat = require('gulp-concat');
const gutil = require('gulp-util');

// Settings
let postCssSettings = [
  autoprefixer({browsers: ['last 2 version']})
];

const config = require('./projectConfig.json');
const devDomain = config.devDomain;

// Tasks

// minify js
gulp.task('compress',function() {
  return gulp.src([
    './library/js/main.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(uglifyjs())
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets/scripts'))
    .pipe(browserSync.stream({match: '**/*.js'}));
});


// Style
gulp.task('style', function() {
  return gulp.src('./library/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(postCssSettings))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./library/css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

// Style build
gulp.task('style-build', function() {
  return gulp.src('./library/scss/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gcmq())
    .pipe(postcss(postCssSettings))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./library/css'));
});

// Sprite
gulp.task('sprite', function() {
  const spritesmith = require('gulp.spritesmith');
  const merge = require('merge-stream');
  const { img, css } = gulp.src('./assets/images/sprite/*.png').pipe(spritesmith({
    imgName: 'spritesheet.png',
    cssName: '_sprite.scss',
    padding: 2,
    cssFormat: 'css',
    imgPath: '../images/spritesheet.png'
  }));
  const imgStream = img.pipe( gulp.dest('./assets/images/') );
  const cssStream = css.pipe( gulp.dest('./assets/sass/') );
  return merge( imgStream, cssStream );
});

// Clean
gulp.task('clean', function() {
  const del = require('del');
  return del(['./library/css/main.css.map']);
});


// Main tasks


// Build
gulp.task('build-dev', gulp.series('clean', 'style', 'compress'));

gulp.task('build', gulp.series('clean', 'style-build' ));



// Watch
gulp.task('serve', gulp.series( 'build-dev', function() {
  browserSync.init({
    files: ['{include,template-parts,woocommerce}/**/*.php', '*.php'],
    proxy: devDomain,
    open: false,
    port: 5050,
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  });
  gulp.watch(['library/scss/**/*.scss'], {cwd: './'}, gulp.series('style'));
  gulp.watch(['library/scss/*.scss'], {cwd: './'}, gulp.series('style'));
  //gulp.watch(['assets/scripts/main.js'], {cwd: './'}, gulp.series('compress'));
  gulp.watch(['library/**/*.{js,jpg,jpeg,gif,png,svg}'], {cwd: './'}, gulp.series('watch:reload'));
  gulp.watch(['library/scss/**/*.scss'], {cwd: './'}, gulp.series('watch:reload'));
}));

gulp.task('add-reload', gulp.series('compress', function() {}));
gulp.task('watch:reload', gulp.series('add-reload', reload));

// Development task
gulp.task('default', gulp.series('serve'));


// Browser reload
function reload(done) {
  browserSync.reload();
  done();
}
