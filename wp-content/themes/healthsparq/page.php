<?php get_header(); the_post();

$header_img = get_post_thumbnail_id($post->ID);
$uuid = uniqid('img');
$classes = [];
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
} else {
	$classes[] = 'no-image';
}

?>

<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
		<header class="entry-header" id="<?php echo $uuid; ?>"></header>
		<div class="entry-content">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php the_content(); ?>
		</div>
	</article>
</div>

<?php get_footer(); ?>

</body>
</html>