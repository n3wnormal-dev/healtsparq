<?php

get_header();
the_post();

$form_id         = get_field( 'form_id' );
$key_file        = get_field( 'key_file' );
$key_file_url    = get_field( 'key_file_url' );
$video_resource  = get_field( 'video_resource' ) ?? 'youtube';
$key_video       = get_field( 'key_video' );
$key_video_vimeo = get_field( 'key_video_vimeo' );
$enable_gate     = get_field( 'enable_gate' );
$gated_content   = get_field( 'gated_content' );

$header_img = get_post_thumbnail_id( $post->ID );
$uuid       = uniqid( 'img' );
$classes    = [];
if ( $header_img )
{
	echo Picture::create( 'style', $header_img, [
		'selector' => '#' . $uuid,
	] );
}
else
{
	$classes[] = 'no-image';
}
if ( $video_resource && ! $enable_gate )
{
	$classes[] = 'video';
}

$terms = get_the_terms( $post->ID, 'resource_type' );
if ( $terms[0]->slug == 'private' )
{
	$term = $terms[1];
}
else
{
	$term = $terms[0];
}

?>

<div class="container">

	<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
		<header class="entry-header <?= $video_resource ?>" id="<?php echo $uuid; ?>">
			<?php if ( $key_video && $video_resource == 'youtube' ): ?>
			<div class="embed-container">
				<div id="player"></div>

				<script>
					var tag = document.createElement( 'script' );
					tag.src = "https://www.youtube.com/iframe_api";
					var firstScriptTag = document.getElementsByTagName( 'script' )[ 0 ];
					firstScriptTag.parentNode.insertBefore( tag, firstScriptTag );

					var player;

					function onYouTubePlayerAPIReady () {
						player = new YT.Player( 'player', {
							// height: '713',
							// width: '100%',
							videoId: '<?php echo $key_video ?>',
							events: {
								// 'onReady': onPlayerReady,
								'onStateChange': onPlayerStateChange
							}
						} );
					}

					// function onPlayerReady(event) {
					// 	/// event.target.playVideo();
					// // console.log('youtube stuff for <?php echo $key_video; ?>');
					// }
					function onPlayerStateChange ( event ) {
						if ( event.data == YT.PlayerState.PLAYING ) {
							gtag( 'event', 'video_play', {
								'event_category': 'Video Play',
								'event_action': player.getVideoUrl(),
								// 'event_label': '<?php echo $term->name; ?>',
							} );
						}
						if ( event.data == YT.PlayerState.ENDED ) {
							gtag( 'event', 'video_complete', {
								'event_category': 'Video Completed',
								'event_action': player.getVideoUrl(),
								// 'event_label': '<?php echo $term->name; ?>',
							} );
						}
					}

				</script>
			</div>
			<?php endif; ?>
			<?php if ( $key_video_vimeo && $video_resource == 'vimeo' ): ?>
			<div class="embed-container">
				<iframe src="https://player.vimeo.com/video/<?= $key_video_vimeo ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<?php endif; ?>
			<style>
				article.type-resources.video .entry-header.vimeo,
				article.type-resources.video .entry-header.youtube {
					height: auto;
				}
				.embed-container {
					--video--width: 1248;
					--video--height: 713;

					position: relative;
					padding-bottom: calc(var(--video--height) / var(--video--width) * 100%); /* 41.66666667% */
					overflow: hidden;
					max-width: 100%;
					/*background: black;*/
				}

				.embed-container iframe {
					position: absolute;
					top: 0;
					left: 0;
					width: 100%;
					height: 100%;
				}
			</style>
		</header>

		<div class="entry-content">

			<?php if ( $term->slug != 'private' ): ?>
				<label class="secondary resource-type" style="background-image:url(<?php the_field( 'secondary_image', $term ); ?>);">
					<?php echo $term->name; ?>
				</label>

			<?php endif; ?>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php if ( ! $term ): ?>
				<div class="author">
					<div class="avatar">
						<?php echo get_avatar( get_the_author_meta( 'email' ) ); ?>
					</div> by <?php the_author_meta( 'display_name' ); ?>
					<i class="clock"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-clock-dark.svg" alt="Posted at" /></i> <?php the_date(); ?>
				</div>
			<?php endif; // term ?>

			<?php if ( $terms[0]->slug != 'private' ):
				echo do_shortcode( '[kiwi-social-bar]' );
			endif; // private ?>
			<li id="email_share" style="display:none;">
				<a data-class="popup" data-network="email" class="kiwi-nw-email" href="mailto:?subject=<?php the_title(); ?>&body=Check this out: <?php the_permalink(); ?>" rel="nofollow"><span class="kicon-envelope"></span>
				</a></li>

			<div id="_content">
				<?php the_content(); ?>
			</div>

			<?php if ( $gated_content ): ?>
				<div id="gated_content" class="hidden">
					<?php echo $gated_content; ?>

					<?php if ( $key_file_url ) { ?>
						<a class="link large key-file" target="_blank" href="<?php echo $key_file_url; ?>">
							<?php the_field( 'key_file_label' ); ?>
						</a>
					<?php } ?>

					<?php if ( ! $key_file_url && $key_file ) { ?>
						<a class="link large key-file" target="_blank" href="<?php echo $key_file; ?>">
							<?php the_field( 'key_file_label' ); ?>
						</a>
					<?php } ?>
				</div>
			<?php endif; ?>

			<?php if ( ! post_password_required() ) { ?>


				<?php if ( $enable_gate && ! isset( $_GET['aliId'] ) ) { ?>
					<script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
				<?php if ( $key_file_url || $key_file ) { ?>

					<form id="mktoForm_<?php echo $form_id; ?>"></form>
					<script>
						MktoForms2.loadForm( "//app-ab09.marketo.com", "130-SXO-349", <?php the_field( 'form_id' ); ?>, function ( form ) {
							//Add an onSuccess handler
							form.onSuccess( function ( values, followUpUrl ) {

								gtag( 'event', 'access_form_submit', {
									'event_category': 'Access Form Submitted',
									'event_action': '<?php if ( $key_file_url )
									{
										echo $key_file_url;
									}
									else
									{
										echo $key_file;
									} ?>',
									'event_label': '<?php echo $term->name; ?>',
								} );
								<?php if ( $term->slug != 'smart-stuff' ) { ?>
								// Take the lead to a different page on successful submit, ignoring the form's configured followUpUrl
								location.href = "<?php if ( $key_file_url )
								{
									echo $key_file_url;
								}
								else
								{
									echo $key_file;
								} ?>";
								<?php } else {  // Resource Type == Smart Stuff ?>
								// Get the form's jQuery element and hide it
								form.getFormElem().hide();
								var preGate = document.getElementById( '_content' );
								var postGate = document.getElementById( 'gated_content' );
								preGate.classList.add( 'hidden' );
								postGate.classList.remove( 'hidden' );
								window.scrollTo( 0, 0 );
								<?php } ?>

								// Return false to prevent the submission handler continuing with its own processing
								return false;
							} );
						} );
					</script>

				<?php } else if ( $key_video || $key_video_vimeo ) { ?>
					<script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
					<form id="mktoForm_<?php echo $form_id; ?>"></form>
					<script>
						MktoForms2.loadForm( "//app-ab09.marketo.com", "130-SXO-349", <?php the_field( 'form_id' ); ?>, function ( form ) {
							//Add an onSuccess handler
							form.onSuccess( function ( values, followUpUrl ) {
								gtag( 'event', 'access_form_submit', {
									'event_category': 'Access Form Submitted',
									<?php if ( $video_resource == 'youtube' ):
										$action = '"https://youtu.be/' . $key_video . '"';
									elseif ( $video_resource == 'vimeo' ) :
										$action = '"https://vimeo.com/' . $key_video_vimeo . '"';
									endif; ?>
									'event_action': <?= $action ?>,
									'event_label': '<?php echo $term->name; ?>',
								} );
								// Get the form's jQuery element and hide it
								form.getFormElem().hide();
								var article = document.getElementById( 'post-<?php the_ID(); ?>' );
								article.classList.remove( 'no-image' );
								article.classList.remove( 'has-post-thumbnail' );
								article.classList.add( 'video' );
								window.scrollTo( 0, 0 );
								// Return false to prevent the submission handler continuing with its own processing
								return false;
							} );
						} );
					</script>

				<?php } else { ?>

					<script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
					<form id="mktoForm_<?php echo $form_id; ?>"></form>
					<script>
						MktoForms2.loadForm( "//app-ab09.marketo.com", "130-SXO-349", <?php the_field( 'form_id' ); ?>, function ( form ) {
							//Add an onSuccess handler
							form.onSuccess( function ( values, followUpUrl ) {

								gtag( 'event', 'access_form_submit', {
									'event_category': 'Access Form Submitted',
									'event_action': '<?php the_title(); ?>',
									'event_label': '<?php echo $term->name; ?>',
								} );
								<?php if ( $term->slug == 'smart-stuff' ) { ?>
								// Get the form's jQuery element and hide it
								form.getFormElem().hide();
								var preGate = document.getElementById( '_content' );
								var postGate = document.getElementById( 'gated_content' );
								preGate.classList.add( 'hidden' );
								postGate.classList.remove( 'hidden' );
								window.scrollTo( 0, 0 );
								<?php } ?>

								// Return false to prevent the submission handler continuing with its own processing
								return false;
							} );
						} );
					</script>

				<?php } ?>


				<?php } else { ?>

				<?php if ( $key_file_url ) { ?>
					<a class="link large key-file" target="_blank" href="<?php echo $key_file_url; ?>">
						<?php the_field( 'key_file_label' ); ?>
					</a>
				<?php } ?>

					<?php if ( ! $key_file_url && $key_file ) { ?>
					<a class="link large key-file" target="_blank" href="<?php echo $key_file; ?>">
						<?php the_field( 'key_file_label' ); ?>
					</a>
				<?php } ?>

				<?php } ?>

			<?php } ?>
		</div>
	</article>
</div>

<?php get_footer(); ?>
<!--00_-iJDFL-g-->
</body>
</html>
