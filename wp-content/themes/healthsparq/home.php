<?php  get_header();

$page_id = get_option('page_for_posts');
$header_img = get_field('header_image',$page_id);
$uuid = uniqid('img');
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
}

?>

<div class="container">

	<header class="landing-header" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<div class="copy">
			<label class="type"><?php echo get_the_title($page_id); ?></label>
			<?php the_field('header_content',$page_id); ?>
		</div>
	</header>

	<section class="filters">
    <div class="grid">
      <div class="col-1-2">
        <?php include('inc/filter_cat.php'); ?>
      </div>
      <div class="col-1-2">
        <?php
					$post_type = 'post';
					get_search_form();
				?>
      </div>
    </div>
  </section>

	<section class="index blog-index layoutPending">
		<div class="block-sizer"></div>
		<div class="gutter-sizer"></div>
		<?php	while ( have_posts() ) : the_post(); ?>
			<?php include('inc/blog-block.php'); ?>
		<?php endwhile; ?>
		<div class="clear"></div>
	</section>

	<?php include('inc/pagination.php'); ?>

</div>



<?php get_footer(); ?>

</body>
</html>
