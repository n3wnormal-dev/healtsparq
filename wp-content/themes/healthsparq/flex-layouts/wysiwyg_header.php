<?php

$img = $block['background_image'];
$uuid = uniqid('img');
if ($img) {
    echo Picture::create('style', $img, array(
        'selector' => '#' . $uuid
    ));
}
$isLastFlex = $totalFlexCount - 1 == $i || $totalBlockCount - 1 == $i;
$isFistFlex = $firstSmallFlexID == $i || $smallBocksIDs == $i;

if ($type_module = 'callout'  && $isFistFlex): ?>
<section class="callouts"><?php endif; ?>

<?php if ($block['link']) { ?>

    <a class="block header wysiwyg" href="<?php echo $block['link']; ?>" id="<?php echo $uuid; ?>">
        <div class="overlay"></div>
        <div class="outer-table">
            <div class="inner-table">
                <?php echo $block['content']; ?>
            </div>
        </div>
    </a>



<?php } else { ?>
    <div class="block header wysiwyg" id="<?php echo $uuid; ?>">
        <div class="overlay"></div>
        <div class="outer-table">
            <div class="inner-table">
                <?php echo $block['content']; ?>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($type_module = 'callout' && $isLastFlex): ?><div class="clear"></div> </section><?php endif; ?>

