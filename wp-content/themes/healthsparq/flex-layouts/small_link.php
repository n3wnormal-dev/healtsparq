<?php

$img = $block['background_image'];
$uuid = uniqid('img');
if ($img) {
	echo Picture::create( 'style', $img, array(
	    'selector' => '#'.$uuid
	) );
}
$type = $block['type_header'];
$desc = $block['description'];
$isLastFlex = $totalFlexCount - 1 == $i || $totalBlockCount - 1 == $i;
$isFistFlex = $firstSmallFlexID == $i || $smallBocksIDs == $i;
?>
<?php if ($type_module = 'callout'  && $isFistFlex): ?>
<section class="callouts"><?php endif; ?>
	<a class="block link" href="<?php echo $block['link']; ?>" target="_blank" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<?php if ($type) { ?>
			<label class="type"><?php echo $type; ?></label>
		<?php } ?>
			<h4><?php echo $block['title']; ?></h4>
	</a>
    <?php if ($desc) { ?>
        <p><?php echo $desc; ?></p>
    <?php } ?>
<?php if ($type_module = 'callout' && $isLastFlex): ?><div class="clear"></div> </section><?php endif; ?>
