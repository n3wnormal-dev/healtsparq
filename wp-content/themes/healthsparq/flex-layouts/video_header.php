<?php
$img = $block['background_image'];
$alt_img = 'https://i1.ytimg.com/vi/' . $block['youtube_video_id'] . '/hqdefault.jpg';
$uuid = uniqid('img');
if ($img != '') {
    echo Picture::create('style', $img, array(
        'selector' => '#' . $uuid
    ));
}

$isLastFlex = $totalFlexCount - 1 == $i || $totalBlockCount - 1 == $i;
$isFistFlex = $firstSmallFlexID == $i || $smallBocksIDs == $i;
?>

<section class="callouts">
    <div class="block video header" style="max-width: -webkit-fill-available">

        <div class="youtube-player" data-id="<?php echo $block['youtube_video_id'] ?>" data-related="0" data-control="2"
             data-info="0" data-fullscreen="0" data-width="auto" data-height="525px"
             data-title="<?php echo $block['title']; ?>" data-origin="<?php echo get_site_url(); ?>">
        </div>

        <div class='thumb'>
            <i class="play"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-play-white.svg"
                                 alt="Play"/></i>
            <h3><?php echo $block['title']; ?></h3>
            <div class="overlay"></div>
            <div class="image" id="<?php echo $uuid ?>"
                 <?php if ($img == '') { ?>style="background-image:url('<?php echo $alt_img ?>');background-size:125%;" <?php } ?>></div>
        </div>

    </div>
    <script type="text/javascript" src="<?php bloginfo( 'template_directory' ); ?>/library/js/video.js"></script>
	<div class="clear"></div> </section>
