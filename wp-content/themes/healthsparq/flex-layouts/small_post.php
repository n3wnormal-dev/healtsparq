<?php

$post = $block['post'];
// $type = $post->post_type;
$isLastFlex = $totalFlexCount - 1 == $i || $totalBlockCount - 1 == $i;
$isFistFlex = $firstSmallFlexID == $i || $smallBocksIDs == $i;

if ($type_module = 'callout' && $isFistFlex): ?><section class="callouts"><?php endif;
if ( $post->post_type == 'resources' ) {
	$terms = get_the_terms($post->ID, 'resource_type');
	$term = $terms[0]->name;
}

if ($post->post_type == 'events') {

	include(get_template_directory().'/inc/event-block.php');

} else if ($post->$post_type != '') {

	$img = get_post_thumbnail_id();
	$uuid = uniqid('img');
	if ($img) {
		echo Picture::create( 'style', $img, array(
		    'selector' => '#'.$uuid
		) );
	}

	?>

	<a class="block post <?php echo $post->post_type; ?>" href="<?php the_permalink(); ?>" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<label class="type"><?php if ($post->post_type == 'post') { echo 'blog'; } else if ($post->post_type == 'resources') { echo $term; } else { echo $post->post_type; } ?></label>
		<h4><?php the_title(); ?></h4>
	</a>

    <?php if($type_module = 'callout' && $isLastFlex): ?> <div class="clear"></div></section><?php endif; ?>
<?php } ?>
