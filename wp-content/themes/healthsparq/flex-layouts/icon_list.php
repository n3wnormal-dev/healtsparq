<?php $goals = $section['item']; ?>
<section class="goals">
  <?php foreach ($goals as $goal) { ?>
    <div class="goal">
      <div class="image" style="background-image:url('<?php echo $goal['icon']; ?>');"></div>
      <div class="copy">
        <h2><?php echo $goal['headline']; ?></h2>
        <?php echo $goal['description']; ?>
      </div>
    </div>
  <?php } ?>
  <div class="clear"></div>
</section>
