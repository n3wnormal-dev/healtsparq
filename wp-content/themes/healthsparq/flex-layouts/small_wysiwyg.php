<?php
$content=$block['content'];
$link=$block['link'];
$img = $block['background_image'];
$uuid = uniqid('img');
$isLastFlex = $totalFlexCount - 1 == $i || $totalBlockCount - 1 == $i;
$isFistFlex = $firstSmallFlexID == $i || $smallBocksIDs == $i;

if ($img) {
	echo Picture::create( 'style', $img, array(
	    'selector' => '#'.$uuid
	) );
}
if ($type_module = 'callout' && $isFistFlex): ?>
<section class="callouts"><?php endif;

if ($link) { ?>

	<a class="block header wysiwyg" href="<?php echo $link; ?>" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<?php echo $content; ?>
	</a>
<!--    <div class="clear"></div>-->
<?php } else { ?>

	<div class="block header wysiwyg" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<?php echo $content; ?>
	</div>

<?php } ?>
    <?php if ($type_module = 'callout' && $isLastFlex): ?> <div class="clear"></div></section><?php endif; ?>
