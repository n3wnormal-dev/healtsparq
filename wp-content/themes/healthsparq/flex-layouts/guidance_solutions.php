<?php
$title=$block['guidance_solutions_title'];
$solutions = $block['solutions'];
?>
<?php if ( $solutions ): ?>
	<section class="guidance-solutions main">
		<h2><?= $title; ?></h2>
		<div class="section-solutions">
			<ul>
				<?php foreach ($solutions as $solution): ?>
					<li class="solution invisible">
						<span class="description"><?= $solution[ 'description' ]; ?></span>
						<span class="wrapper-image">
								  <img src="<?= $solution[ 'icon' ]['url']; ?>" alt="">
							</span>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</section>
<?php endif; ?>
