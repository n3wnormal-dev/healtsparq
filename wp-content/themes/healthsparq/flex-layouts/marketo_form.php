<?php $blocks = $block['marketo_form']; ?>
<?php $form_id = $block['form_id']; ?>

<section class="main marketo-form-wrapper">
	<div class="inner">
		<div id="_content">
			<?= $block['text_field']; ?>
		</div>

		<?php if ( $block['success_content'] ): ?>
			<div id="success_content">
				<?= $block['success_content']; ?>
			</div>
		<?php endif; ?>

		<?php if ( $form_id && ! isset( $_GET['aliId'] ) ) { ?>
			<script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
		<?php if ( $block['success_content'] ) { ?>

			<form id="mktoForm_<?php echo $form_id; ?>"></form>
			<script>
				MktoForms2.loadForm( "//app-ab09.marketo.com", "130-SXO-349", <?php echo $form_id; ?>, function ( form ) {
					//Add an onSuccess handler
					form.onSuccess( function ( values, followUpUrl ) {

						<?php if (is_page( 'contact' )) { ?>
						gtag( 'event', 'contact_form_submit', {
							'event_category': 'Contact Form Submitted',
						} );
						<?php } else if (is_page( 'demo' )) { ?>
						gtag( 'event', 'demo_form_submit', {
							'event_category': 'Demo Request Form Submitted',
						} );
						<?php }?>

						// Get the form's jQuery element and hide it
						form.getFormElem().hide();
						var content = document.getElementById( '_content' );
						var msg = document.getElementById( 'success_content' );
						content.classList.add( 'hidden' );
						msg.classList.add( 'visible' );
						// window.scrollTo( 0, 0 );
						// Return false to prevent the submission handler continuing with its own processing
						return false;
					} );
				} );
			</script>

		<?php } else { ?>

			<script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
			<form id="mktoForm_<?php echo $form_id; ?>"></form>
			<script>
				MktoForms2.loadForm( "//app-ab09.marketo.com", "130-SXO-349", <?php echo $form_id; ?>, function ( form ) {
					//Add an onSuccess handler
					form.onSuccess( function ( values, followUpUrl ) {

						<?php if (is_page( 'contact' )) { ?>
						gtag( 'event', 'contact_form_submit', {
							'event_category': 'Contact Form Submitted',
						} );
						<?php } else if (is_page( 'demo' )) { ?>
						gtag( 'event', 'demo_form_submit', {
							'event_category': 'Demo Request Form Submitted',
						} );
						<?php }?>

					} );
				} );
			</script>
		<?php } ?>

		<?php } ?>
	</div>
</section>


<style>
	#success_content {
		display: none;
		overflow: hidden;
		opacity: 0;
		height: 0;
	}

	#success_content.visible {
		display: block;
		overflow: visible;
		opacity: 1;
		height: auto;
	}
</style>
