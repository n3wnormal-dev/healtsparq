<?php $blocks = $block['why_healthsparq']; ?>

<?php if ( $blocks): ?>
	<div class="section-why-health-sparq">
		<h2><?= $block['why_health_title']; ?></h2>
		<ul>
			<?php foreach ( $blocks as $block ) :  ?>
				<li class="why-health-sparq" style="justify-content: space-between">
					<span class="wrapper-image" style="height: auto">
							<img src="<?= $block[ 'image' ]['url']; ?>" alt="">
					</span>
					<span class="meta">
						<span class="title">
							<?= $block[ 'title' ]; ?>
						</span>
						<span class="description"><?= $block[ 'description' ]; ?></span>
					</span>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php if (get_field('cta_button') && $block['why_health_show_demo_button']): ?>
			<p style="text-align: center;"><a class="btn hidden" href="<?= get_field('cta_button')['url'] ?>"><?= get_field('cta_button')['title'] ?></a></p>
		<?php endif; ?>
	</div>
<?php endif; ?>
