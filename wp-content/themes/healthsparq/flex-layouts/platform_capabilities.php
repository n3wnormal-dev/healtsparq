<?php $capabilities = $block['capabilities']; ?>
<?php if ( $capabilities ): ?>
	<div class="section-capabilities">
		<h3 style="margin-top: 10px"><?= $block[ 'capabilities_title' ]; ?></h3>
		<ul>
			<?php foreach ( $capabilities as $capability ): ?>
				<li class="capability">
					<?php if ($capability[ 'link' ]): ?>
					<a href="<?= $capability[ 'link' ]['url']; ?>">
						<?php endif; ?>
						<span class="wrapper-image">
								  <img src="<?= $capability['image']['url']; ?>" alt="">
						</span>
					<span class="meta">
							<span class="title"><?= $capability[ 'title' ]; ?></span>
							<span class="description"><?= $capability[ 'description' ]; ?></span>
						</span>
						<?php if ($capability[ 'link' ]): ?></a><?php endif; ?>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php if (get_field('cta_button') && $block['show_demo_button']): ?>
			<p style="text-align: center;"><a class="btn hidden" href="<?= get_field('cta_button')['url'] ?>"><?= get_field('cta_button')['title'] ?></a></p>
		<?php endif; ?>
	</div>
<?php endif; ?>
