<?php

$post = $block['post'];
$img = get_post_thumbnail_id();
$uuid = uniqid('img');
if ($img) {
	echo Picture::create( 'style', $img, array(
	    'selector' => '#'.$uuid
	) );
}
$type_module;

if ($post->post_type == 'events' && get_field('no_link')) { ?>
<?php if($type_module = 'callout'): ?><section class="callouts"><?php endif; ?>
	<div class="block post wide <?php echo $post->post_type; ?>">
		<div class="copy">
			<div class="type"><?php echo $post->post_type; ?></div>
			<h4><?php the_title(); ?></h4>
		</div>
		<div class="background" id="<?php echo $uuid; ?>"></div>
	</div>
	<?php if($type_module = 'callout'): ?> </section><?php endif; ?>
<?php } else {

	$link = get_permalink();
	if (get_post_format() == 'link') $link = get_field('external_url');

	if ( $post->post_type == 'resources' ) {
		$terms = get_the_terms($post->ID, 'resource_type');
		$term = $terms[0]->name;
	}
	?>
	<?php if($type_module = 'callout'): ?><section class="callouts"><?php endif; ?>
	<a class="block post wide <?php echo $post->post_type; ?>" href="<?php echo $link; ?>">
		<div class="copy">
			<label class="type"><?php if ($post->post_type == 'post') { echo 'blog'; } else if($post->post_type == 'resources') { echo $term; } else { echo $post->post_type; } ?></label>
			<h4><?php the_title(); ?></h4>
		</div>
		<div class="background" id="<?php echo $uuid; ?>"></div>
	</a>
    <div class="clear"></div>
	<?php if($type_module = 'callout'): ?> </section><?php endif; ?>
<?php } ?>
