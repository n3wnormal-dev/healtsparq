<?php
$title = $block['in_action_title'];
$content = $block['in_action_content'];
$background = $block['in_action_image'];
?>

<div class="in-action">
	<div class="column">
		<h4><?= $title; ?></h4>
		<?= $content; ?>
	</div>
	<div class="column" style='background: url(<?= $background['url']?>) 50% 50% no-repeat'></div>
</div>
