<?php $blocks = $block['features']; ?>
<?php if ( $blocks ): ?>
	<div class="platform-section">
		<h2><?php echo $block[ 'features_title' ]; ?></h2>
		<div class="section-features">
			<ul>
				<?php foreach ($blocks as $block): ?>
					<li>
						<?php $link = $block[ 'link' ] ? $block[ 'link' ]['url']: '#'; ?>
						<a href="<?= $link ?>">
							<span class="wrapper-image" style="height: auto">
							  <img src="<?= $block[ 'icon' ]['url']; ?>" alt="">
							</span>
							<span class="description"><?= $block[ 'description' ]; ?></span>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>
