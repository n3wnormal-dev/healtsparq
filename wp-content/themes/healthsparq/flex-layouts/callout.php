<?php
  $callout = $section['callout'];
  $img = $section['image'];
  $uuid = uniqid('img');
  if ($img) {
    echo Picture::create( 'style', $img, array(
        'selector' => '#'.$uuid
    ) );
  }

?>

<section class="tout">
  <div class="image" id="<?php echo $uuid; ?>"><span class="invisible"><?php echo rwp_img( $img ); ?></span></div>
  <div class="copy">
    <?php echo $section['copy']; ?>
  </div>
</section>
