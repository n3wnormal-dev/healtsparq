<?php
/*
Template Name: Resources
*/
get_header();

$args = array(
	'post_type' => 'resources',
	'posts_per_page' => -1,
	'tax_query' => array(
    array(
        'taxonomy'  => 'resource_type',
        'field'     => 'slug',
        'terms'     => 'private', // exclude items media items in the news-cat custom taxonomy
        'operator'  => 'NOT IN'
			),
   )
);
$posts = new WP_Query($args);

?>

<div class="container">

	<?php include('inc/resource-header.php'); ?>

	<section class="index resource-index layoutPending">
		<div class="block-sizer"></div>
		<div class="gutter-sizer"></div>
		<?php while ($posts->have_posts()) : $posts->the_post(); ?>
			<?php include('inc/resource-block.php'); ?>
		<?php endwhile; ?>
		<div class="clear"></div>
	</section>
	<?php //include('inc/pagination.php'); ?>
	<?php wp_reset_query(); ?>
</div>

<?php get_footer(); ?>

</body>
</html>
