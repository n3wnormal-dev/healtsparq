<?php

get_header();
the_post();

$header_img = get_field('alternate_header_image');
$feat_img = get_post_thumbnail_id($post->ID);
$uuid = uniqid('img');
$classes = [];
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
} elseif (!$header_img && $feat_img) {
	echo Picture::create( 'style', $feat_img, array(
	    'selector' => '#'.$uuid
	) );
} else {
	$classes[] = 'no-image';
}

?>

<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>

		<header class="entry-header" id="<?php echo $uuid; ?>"></header>

		<div class="entry-content">

			<?php if ($post->post_type == 'post' && is_active_sidebar( 'blog_sidebar' ) ) : ?>
				<div class="sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'blog_sidebar' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php if (!$term  && $post->post_type == 'post') { ?>
				<div class="author">
					<?php if ($author != 'Anonymous') echo '<a href="#author">'; ?>
						<?php
						$author_id = get_the_author_meta('ID');
						$author_img = get_field('profile_image', 'user_'. $author_id );
						if ( $author_img != '' ) { ?>
							<img class="avatar" src="<?php echo $author_img['url']; ?>" alt="<?php echo $author_img['alt']; ?>" />
						<?php }
						else {
							echo get_avatar( get_the_author_meta('email') );
						} ?>
					 by <?php the_author_meta('display_name'); ?> <i class="clock"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-clock-dark.svg" alt="Posted at" /></i> <?php the_date(); ?>
					<?php if ($author != 'Anonymous') echo '</a>'; ?>
				</div>
			<?php } ?>

			<?php echo do_shortcode('[kiwi-social-bar]'); ?>
			<li id="email_share" style="display:none;"><a data-class="popup" data-network="email" class="kiwi-nw-email" href="mailto:?subject=<?php the_title(); ?>&body=Check this out: <?php the_permalink(); ?>" rel="nofollow"><span class="kicon-envelope"></span> </a></li>

			<?php the_content(); ?>

		</div>

		<footer>

			<?php if ($post->post_type == 'post') { ?>
				<section id="comments">
			    <?php comments_template(); ?>
			  </section>
			<?php } ?>

			<?php //if ( $author != 'Anonymous' && $post->post_type == 'post') {  ?>
		    <section class="author author-bio" id="author">
					<!-- <div class="avatar"> -->
						<?php
						if ( $author_img != '' ) { ?>
							<img class="avatar" src="<?php echo $author_img['url']; ?>" alt="<?php echo $author_img['alt']; ?>" />
						<?php }
						else {
							echo get_avatar( get_the_author_meta('email'), 80 );
						} ?>
					<!-- </div> -->
		      <div class="info">
		        <h5><?php the_author_meta('display_name'); ?></h5>
		        <h6><?php the_author_meta( 'description' ); ?></h6>
		      </div>
		    </section>
		  <?// } ?>
			<nav class="grid">

		    <div class="prev col-1-2">
		      <?php previous_post_link('%link', '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/icons/ico-page-prev.svg" /><div><h4>Previous</h4><h5>%title</h5></div>'); ?>
		    </div>
		    <div class="next col-1-2">
		      <?php next_post_link('%link', '<div><h4>Next</h4><h5>%title</h5></div><img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/icons/ico-page-next.svg" />'); ?>
	      </div>

		  </nav>
		</footer>
	</article>
</div>

<?php get_footer(); ?>

</body>
</html>
