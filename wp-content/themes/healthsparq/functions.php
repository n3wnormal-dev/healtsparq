<?php
/*
Author: Eddie Machado
URL: http://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, etc.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/setup.php' );

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
require_once( 'library/admin.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  //Allow editor style.
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

  // let's get language support going, if you need it
  // load_theme_textdomain( 'bonestheme', get_template_directory() . '/library/translation' );

  // USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
  require_once( 'library/custom-post-types.php' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  // add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  // add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action('init', 'replace_jquery');
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // add_action( 'get_footer', 'bones_styles_in_footer' );

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  // add_action( 'widgets_init', 'bones_register_sidebars' );

// Custom Permalinks for Resource Types
  add_filter( 'post_type_link', 'wpa_resource_permalinks', 1, 2 );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt

  add_filter( 'excerpt_more', 'bones_excerpt_more' );
  add_filter('excerpt_length', 'custom_excerpt_length', 999);
} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );

function wpb_adding_scripts() {
  wp_register_script('mega_menu_script', plugin_dir_url('megamenu' ,(__FILE__)) . 'megamenu/js/maxmegamenu.js');

  wp_enqueue_script('mega_menu_script');
}
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );

/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
// add_image_size( 'bones-thumb-600', 600, 150, true );
// add_image_size( 'bones-thumb-300', 300, 100, true );
// add_image_size( 'bones-thumb-600x500', 600, 500, true );
// add_image_size( 'bones-thumb-400x400', 400, 400, true );
// add_image_size( 'bones-thumb-250x190', 250, 190, true );
add_image_size('single',1300);
add_image_size('double',2600);
add_image_size('half',750);
add_image_size('third',450);

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

// add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );
//
// function bones_custom_image_sizes( $sizes ) {
//     return array_merge( $sizes, array(
//         'bones-thumb-600' => __('600px by 150px'),
//         'bones-thumb-300' => __('300px by 100px'),
//         'bones-thumb-600x500' => __('600px by 500px'),
//         'bones-thumb-400x400' => __('400px by 400px'),
//         'bones-thumb-250x190' => __('250px by 190px')
//         // 'bones-thumb-300' => __('300px by 100px'),
//     ) );
// }

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* THEME CUSTOMIZE *********************/

/*
  A good tutorial for creating your own Sections, Controls and Settings:
  http://code.tutsplus.com/series/a-guide-to-the-wordpress-theme-customizer--wp-33722

  Good articles on modifying the default options:
  http://natko.com/changing-default-wordpress-theme-customization-api-sections/
  http://code.tutsplus.com/tutorials/digging-into-the-theme-customizer-components--wp-27162

  To do:
  - Create a js for the postmessage transport method
  - Create some sanitize functions to sanitize inputs
  - Create some boilerplate Sections, Controls and Settings
*/

// function bones_theme_customizer($wp_customize) {
  // $wp_customize calls go here.
  //
  // Uncomment the below lines to remove the default customize sections

  // $wp_customize->remove_section('title_tagline');
  // $wp_customize->remove_section('colors');
  // $wp_customize->remove_section('background_image');
  // $wp_customize->remove_section('static_front_page');
  // $wp_customize->remove_section('nav');

  // Uncomment the below lines to remove the default controls
  // $wp_customize->remove_control('blogdescription');

  // Uncomment the following to change the default section titles
  // $wp_customize->get_section('colors')->title = __( 'Theme Colors' );
  // $wp_customize->get_section('background_image')->title = __( 'Images' );
// }

// add_action( 'customize_register', 'bones_theme_customizer' );

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function blog_widgets_init() {

	register_sidebar( array(
		'name'          => 'Blog Sidebar',
		'id'            => 'blog_sidebar',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'blog_widgets_init' );

add_filter('dynamic_sidebar_params', 'my_dynamic_sidebar_params');

function my_dynamic_sidebar_params( $params ) {

	// get widget vars
	$widget_name = $params[0]['widget_name'];
	$widget_id = $params[0]['widget_id'];


	// bail early if this widget is not an Image widget
	if( $widget_name != 'Image' ) {

		return $params;

	}

	// Add layout type
	$layout = get_field('layout', 'widget_' . $widget_id);

	if( $layout ) {

		// $params[0]['before_widget'] .= '<style type="text/css">';
		// $params[0]['before_widget'] .= sprintf('#%s-copy { background-color: %s; }', $widget_id, $color);
		// $params[0]['before_widget'] .= '</style>';

		$layout_type = $layout; // make sure you leave a space at the end
		$new_classes = 'class="'.$layout_type.' ';
		$params[0]['before_widget'] = str_replace('class="',$new_classes,$params[0]['before_widget']);

	}

	// add color style to before_widget
	$color = get_field('background_color', 'widget_' . $widget_id);

	if( $color ) {

		$params[0]['before_widget'] .= '<style type="text/css">';
		$params[0]['before_widget'] .= sprintf('#%s-copy { background-color: %s; }', $widget_id, $color);
		$params[0]['before_widget'] .= sprintf('#%s-overlay { background-color: %s; }', $widget_id, $color);
		$params[0]['before_widget'] .= '</style>';

	}

	// add copy to after_widget
	$copy = get_field('callout_copy', 'widget_' . $widget_id);

	if( $copy ) {

		$params[0]['after_widget'] = '<div class="copy" id="'.$widget_id.'-copy">'.$copy.'</div>'. $params[0]['after_widget'];

	}

	// add subhead and link to after_widget
	$subhead = get_field('subhead', 'widget_' . $widget_id);
	$link = get_field('link', 'widget_' . $widget_id);

	if( $subhead ) {

		$params[0]['after_widget'] = '<h5>'.$subhead.'</h5><div class="overlay" id="'.$widget_id.'-overlay"></div><a href="'.$link.'"></a>'. $params[0]['after_widget'];

	}

	// return
	return $params;

}

add_filter('dynamic_sidebar_params', 'layout_type');

function layout_type($params){
    if ($params[0]['widget_id'] == "my-widget-1"){ //make sure its your widget id here
        // its your widget so you add  your classes
        $classe_to_add = 'col480 whatever bla bla '; // make sure you leave a space at the end
        $classe_to_add = 'class=" '.$classe_to_add;
        $params[0]['before_widget'] = str_replace('class="',$classe_to_add,$params[0]['before_widget']);
    }
    return $params;
}


	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
// } // don't remove this bracket!



/* DON'T DELETE THIS CLOSING TAG */ ?>
