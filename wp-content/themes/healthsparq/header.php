<!DOCTYPE html>
<html <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">

		<?php include('inc/favicons.php'); ?>

		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


		<!-- <script src="https://use.typekit.net/jdc7vru.js"></script> -->
		<script>try{Typekit.load({ async: true });}catch(e){}</script>

		<?php wp_head(); ?>

		<?php // drop Google Analytics Here ?>
		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-35846420-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments)};
		  gtag('js', new Date());

		  gtag('config', 'UA-35846420-1');
		</script>
		<meta name="google-site-verification" content="jwDd9_4j5I1tq8arpmq0ZbialEMcYXogktVEVkL5Zu8" />
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?>>

	<?php
	$current_page_ID = get_the_ID();
	$pages_list      = get_field( 'pages_list', 'options' );
	$inList          = '';
	foreach ( $pages_list as $item )
	{
		if ( $item->ID == $current_page_ID )
		{
			$inList = true;
		}
	}
	if (get_field( 'show_notification_bar', 'options') && $inList):
		get_template_part( 'inc/notification', 'bar');
	endif; ?>

	<?php if (is_404()) { ?>
		<header>
			<div class="container no-nav">
				<a href="<?php echo get_home_url(); ?>" class="logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg" alt="HealthSparq">
				</a>
			</div>
		</header>
	<?php } else { ?>
		<header class="top-bar <?php if (is_front_page()) echo 'special'; ?>">

			<?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'menu_class' => 'nav-menu') ); ?>

<!--
				<button id="hamburger" type="button" class="mobile hamburger tcon tcon-menu--xbutterfly" aria-label="toggle menu">
				  <span class="tcon-menu__lines" aria-hidden="true"></span>
				  <span class="tcon-visuallyhidden">toggle menu</span>
				</button> -->


		</header>
	<?php } ?>
