<?php get_header(); the_post(); ?>

<div class="container">

	<?php

		$hero_img = get_field('hero_background');
		$uuid = uniqid('img');
		if ($hero_img) {
			echo Picture::create( 'style', $hero_img, array(
			    'selector' => '#'.$uuid
			) );
		}

		$hero_text = get_field('hero_text');

	?>

	<header class="hero" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<div class="copy">
			<?php if ($hero_text) { the_field('hero_text'); } ?>
		</div>

		<div class="feature qa-slider flexslider">
			<ul class="slides">
				<?php $qas = get_field('qanda'); ?>
				<?php
				// check if the repeater field has rows of data
				if( have_rows('qanda') ):
					// loop through the rows of data
					while ( have_rows('qanda') ) : the_row(); ?>

						<li class="qa-slide">
							<span class="question" data-question="<?php the_sub_field('question'); ?>">
								<?php the_sub_field('question'); ?>
							</span>
							<div class="answer"><img src="<?php the_sub_field('answer'); ?>" alt="" /></div>
							<!-- <div class="fake-btn">Enter</div> -->
						</li>

		    	<?php endwhile;
				else :
		    // no rows found
				endif; ?>
		</ul>
				<!-- <div class="btn">Enter</div> -->
		</div>
		<!-- <i class="scroll"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/ico-hmpg-scroll.svg" alt="Scroll, please" /></i> -->
	</header>


	<?php

		$talking_img = get_field('talking_background');
		$uuid = uniqid('img');
		if ($talking_img) {
			echo Picture::create( 'style', $talking_img, array(
			    'selector' => '#'.$uuid
			) );
		}

		$points = get_field('talking_points');

	?>

	<section class="talking-points" id="<?php echo $uuid; ?>">
		<div class="copy">
			<h2><?php the_field('talking_header'); ?></h2>
			<div class="points">
				<?php foreach ($points as $point) { ?>
					<div class="point">
						<?php echo $point['talking_point']; ?>
					</div>
				<?php } ?>
			</div>
			<?php the_field('talking_link'); ?>
		</div>
	</section>

	<?php

		$numbers_img = get_field('numbers_background');
		$uuid = uniqid('img');
		if ($numbers_img) {
			echo Picture::create( 'style', $numbers_img, array(
			    'selector' => '#'.$uuid
			) );
		}

		$numbers = get_field('numbers');

	?>

	<section class="numbers" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<h2><?php the_field('numbers_header'); ?></h2>
		<p><?php the_field('numbers_subheader'); ?></p>
		<?php foreach ($numbers as $number) { ?>
			<div class="number">
				<div class="figure"><span class="odometer" data-value="<?php echo $number['number']; ?>">0</span><span><?php echo $number['number_format']; ?></span></div>
				<label><?php echo $number['label']; ?></label>
				<!-- <svg class="animatedBorder" viewbox="0 0 100 100">
				  <circle cx="50" cy="50" r="45" fill="none"/>
				  <path class="progress" stroke-linecap="round" stroke-width="1" stroke="#fff" fill="none"
				        d="M50 10
				           a 40 40 0 0 1 0 80
				           a 40 40 0 0 1 0 -80">
				  </path>
				</svg> -->
			</div>
		<?php } ?>
	</section>



	<section class="dynamic">
		<div class="copy"><?php the_content(); ?></div>

		  <?php
		    $dynamic_img = get_field('key_image');
		    $uuid = uniqid('img');
		    if ($dynamic_img) {
		      echo Picture::create( 'style', $dynamic_img, array(
		          'selector' => '#'.$uuid
		      ) );
		    }
		  ?>
		  <div class="image static" id="<?php echo $uuid; ?>">
		  </div>

	</section>



</div><!-- /.container -->

<?php $blocks = get_field('callouts'); ?>
<?php if ($blocks) { ?>
<section class="callouts">
  <h2><?php the_field('section_heading'); ?></h2>
  <?php
    $i = 0;
    $layouts = array();
    foreach ($blocks as $block) {
      include('flex-layouts/'.$block['acf_fc_layout'].'.php');
      $layouts[] = $block['acf_fc_layout'];
      $i++;
    }
    // print_r($layouts);
  ?>
  <div class="clear"></div>
  <?php if ( in_array('video_header', $layouts)  || in_array('small_video', $layouts) ) { ?>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/library/js/video.js"></script>
  <?php } ?>
</section>
</section>
<?php } ?>


<?php get_footer(); ?>

</body>
</html>
