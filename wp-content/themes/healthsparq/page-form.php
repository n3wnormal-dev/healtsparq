<?php
/*
Template Name: Form
*/
get_header(); the_post();

$header_img = get_post_thumbnail_id($post->ID);
$uuid = uniqid('img');
$classes = [];
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
} else {
	$classes[] = 'no-image';
}

$form_id = get_field('form_id');
$success_content = get_field('success_content');

?>

<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
		<header class="entry-header" id="<?php echo $uuid; ?>"></header>
		<div class="entry-content">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div id="_content">
				<?php the_content(); ?>
			</div>
			<div id="success_content">
				<?php echo $success_content; ?>
			</div>

			<?php if ($form_id && !isset($_GET['aliId'])) { ?>
				<script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
				<?php if ($success_content) { ?>

					<form id="mktoForm_<?php echo $form_id; ?>"></form>
					<script>
					MktoForms2.loadForm("//app-ab09.marketo.com", "130-SXO-349", <?php echo $form_id; ?>, function(form) {
						//Add an onSuccess handler
						form.onSuccess(function(values, followUpUrl) {

							<?php if (is_page('contact')) { ?>
								gtag('event', 'contact_form_submit', {
								  'event_category': 'Contact Form Submitted',
								});
							<?php } else if (is_page('demo')) { ?>
								gtag('event', 'demo_form_submit', {
									'event_category': 'Demo Request Form Submitted',
								});
							<?php }?>

							// Get the form's jQuery element and hide it
							form.getFormElem().hide();
							var content = document.getElementById('_content');
							var msg = document.getElementById('success_content');
							content.classList.add('hidden');
							msg.classList.add('visible');
							window.scrollTo(0, 0);
							// Return false to prevent the submission handler continuing with its own processing
							return false;
						});
					});
					</script>

				<?php } else { ?>

					<script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
					<form id="mktoForm_<?php echo $form_id; ?>"></form>
					<script>
					MktoForms2.loadForm("//app-ab09.marketo.com", "130-SXO-349", <?php echo $form_id; ?>, function(form) {
						//Add an onSuccess handler
						form.onSuccess(function(values, followUpUrl) {

							<?php if (is_page('contact')) { ?>
								gtag('event', 'contact_form_submit', {
								  'event_category': 'Contact Form Submitted',
								});
							<?php } else if (is_page('demo')) { ?>
								gtag('event', 'demo_form_submit', {
									'event_category': 'Demo Request Form Submitted',
								});
							<?php }?>

						});
					});
					</script>
				<?php } ?>

			<?php } ?>

			<?php if (is_page('contact')): ?>
				<div class="contact-details">
					<div class="grid">
						<?php if (get_field('mailing_address')): ?>
							<div class="col-1-2">
								<h3>Mailing Address</h3>
								<p><?php the_field('mailing_address'); ?></p>
							</div>
						<?php endif; ?>
						<?php if (get_field('phone')): ?>
							<div class="col-1-2">
								<h3>Phone</h3>
								<p><?php the_field('phone'); ?></p>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>

		</div>
	</article>
</div>

<?php get_footer(); ?>

</body>
</html>
