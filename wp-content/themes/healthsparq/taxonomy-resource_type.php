<?php get_header();

$header_img = get_field('header_image');
$uuid = uniqid('img');
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
}

?>

<div class="container">

	<?php include('inc/resource-header.php'); ?>

	<section class="index resource-index layoutPending">
		<div class="block-sizer"></div>
		<div class="gutter-sizer"></div>
		<?php while (have_posts()) : the_post(); ?>
			<?php include('inc/resource-block.php'); ?>
		<?php endwhile; wp_reset_query(); ?>
		<div class="clear"></div>
	</section>

</div>

<?php get_footer(); ?>

</body>
</html>
