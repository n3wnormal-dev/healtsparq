<?php

get_header();
the_post();

$header_img = get_field('alternate_header_image');
$feat_img = get_post_thumbnail_id($post->ID);
$uuid = uniqid('img');
$classes = [];
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
} elseif (!$header_img && $feat_img) {
	echo Picture::create( 'style', $feat_img, array(
	    'selector' => '#'.$uuid
	) );
} else {
	$classes[] = 'no-image';
}

?>

<div class="container">

	<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>

		<header class="entry-header" id="<?php echo $uuid; ?>"></header>

		<div class="entry-content">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php echo do_shortcode('[kiwi-social-bar]'); ?>
			<li id="email_share" style="display:none;"><a data-class="popup" data-network="email" class="kiwi-nw-email" href="mailto:?subject=<?php the_title(); ?>&body=Check this out: <?php the_permalink(); ?>" rel="nofollow"><span class="kicon-envelope"></span> </a></li>
			
			<?php the_content(); ?>
		</div>

		<footer>

			<nav class="grid">
		    <div class="prev col-1-2">
		    	<a href="/about#leadership">
		    		<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/icons/ico-page-prev.svg" /><div><h4>Back</h4><h5>to Leadership Section</h5></div>
		    	</a>
		    </div>
		  </nav>

		</footer>

	</article>

</div>

<?php get_footer(); ?>

</body>
</html>
