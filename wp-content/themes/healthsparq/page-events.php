<?php
/*
Template Name: Events
*/
get_header();

$args = array(
	'post_type' => 'events',
	'posts_per_page' => -1
);
$posts = new WP_Query($args);
$count = $posts->post_count;

?>

<div class="container">

	<?php include('inc/landing-header.php'); ?>

	<section class="events">
		<?php
			$i = 0;
			while ($posts->have_posts()) {
				$posts->the_post();
				include('inc/event-block.php');
				$i++;
			}
			wp_reset_query();
		?>
		<div class="clear"></div>
	</section>

</div>

<?php get_footer(); ?>

</body>
</html>
