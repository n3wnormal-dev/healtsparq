<?php

get_header();
the_post();

$header_img = get_field('alternate_header_image');
$feat_img = get_post_thumbnail_id($post->ID);
$uuid = uniqid('img');
$classes = [];
if ($header_img) {
	echo Picture::create( 'style', $header_img, array(
	    'selector' => '#'.$uuid
	) );
} elseif (!$header_img && $feat_img) {
	echo Picture::create( 'style', $feat_img, array(
	    'selector' => '#'.$uuid
	) );
} else {
	$classes[] = 'no-image';
}

?>

<div class="container">

	<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>

		<header class="entry-header" id="<?php echo $uuid; ?>"></header>

		<div class="entry-content">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php echo do_shortcode('[kiwi-social-bar]'); ?>
			<li id="email_share" style="display:none;"><a data-class="popup" data-network="email" class="kiwi-nw-email" href="mailto:?subject=<?php the_title(); ?>&body=Check this out: <?php the_permalink(); ?>" rel="nofollow"><span class="kicon-envelope"></span> </a></li>

			<?php the_content(); ?>
		</div>

		<footer>

			<section class="press-contact">
				<?php if( have_rows('press_contact','option') ): ?>
					<h6>Press Contact</h6>
					<div class="grid">

					<?php while( have_rows('press_contact','option') ): the_row();

						// vars
						$fname = get_sub_field('first_name');
						$lname = get_sub_field('last_name');
						$phone = get_sub_field('phone');
						$email = get_sub_field('email');
						$button = get_sub_field('button_text');

						?>

						<div class="col-1-3">


							<h5><?php echo $fname . ' ' . $lname; ?></h5>
							<p><?php echo $phone ?></p>
							<a class="btn tertiary small" href="mailto:<?php echo $email; ?>"><?php if( $button ) { echo $button; } else { echo 'Email ' . $fname; }?></a>

						</div>

					<?php endwhile; ?>

				</div>

				<?php endif; ?>
			</section>

			<nav class="grid">
		    <div class="prev col-1-2">
		      <?php previous_post_link('%link', '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/icons/ico-page-prev.svg" /><div><h4>Previous</h4><h5>%title</h5></div>'); ?>
		    </div>
		    <div class="next col-1-2">
		      <?php next_post_link('%link', '<div><h4>Next</h4><h5>%title</h5></div><img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/icons/ico-page-next.svg" />'); ?>
				</div>
		  </nav>

		</footer>

	</article>

</div>

<?php get_footer(); ?>

</body>
</html>
