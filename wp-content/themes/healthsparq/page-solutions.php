<?php
/*
Template Name: Solutions Landing
*/
get_header(); ?>

<div class="container">

	<?php

		$header_img = get_field('header_image');
		$uuid = uniqid('img');
		if ($header_img) {
			echo Picture::create( 'style', $header_img, array(
			    'selector' => '#'.$uuid
			) );
		}

		$children = wp_list_pages(array(
		  'child_of' => $post->ID,
		  'depth' => 1,
		  'echo' => false,
		  'title_li' => null
		));


	?>

	<header class="landing-header" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<div class="copy">
			<label class="type"><?php the_title(); ?></label>
			<?php the_field('header_content'); ?>
		</div>
	</header>

	<?php $goals = get_field('strategic_goals'); ?>

	<section class="goals">
		<?php foreach ($goals as $goal) { ?>
			<div class="goal">
				<div class="image" style="background-image:url(<?php echo $goal['icon']; ?>);"></div>
				<div class="copy">
					<h2><?php echo $goal['headline']; ?></h2>
					<?php echo $goal['description']; ?>
				</div>
			</div>
		<?php } ?>
		<div class="clear"></div>
	</section>


	<?php

		$img = get_field('callout_image');
		$uuid = uniqid('img');
		if ($img) {
			echo Picture::create( 'style', $img, array(
			    'selector' => '#'.$uuid
			) );
		}

	?>

	<section class="tout" id="<?php echo $uuid; ?>">
		<div class="copy">
			<?php the_field('callout_copy'); ?>
		</div>
	</section>


	<section class="main">
		<?php the_content(); ?>
	</section>

	<?php $blocks = get_field('callouts'); ?>
	<?php if ($blocks) { ?>
	<section class="callouts">
	  <h2><?php the_field('section_heading'); ?></h2>
	  <?php
	    $i = 0;
	    $layouts = array();
	    foreach ($blocks as $block) {
	      include('flex-layouts/'.$block['acf_fc_layout'].'.php');
	      $layouts[] = $block['acf_fc_layout'];
	      $i++;
	    }
	    // print_r($layouts);
	  ?>
	  <div class="clear"></div>
	  <?php if ( in_array('video_header', $layouts)  || in_array('small_video', $layouts) ) { ?>
	    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/library/js/video.js"></script>
	  <?php } ?>
	</section>
	<?php } ?>

</div>

<?php get_footer(); ?>

</body>
</html>
