<?php get_header(); ?>

<div class="container results-container">
	<section class="main">
		<div class="entry-content">
			<header>
				<small>Search Results for:</small>
				<h1><?php echo get_search_query(); ?></h1>
			</header>
			<?php

      if ( have_posts() ) :
        /* Start the Loop */
        while ( have_posts() ) : the_post();

          /**
           * Run the loop for the search to output the results.
           */
          include('inc/search-result.php');

        endwhile; // End of the loop.

      else : ?>

        <h2>Sorry, but nothing matched your search terms. Please try again with some different keywords.</h2>
        <?php get_search_form(); ?>
      <?php endif; ?>
		</div>

    <?php include('inc/pagination.php'); ?>
	</section>
</div>

<?php get_footer(); ?>

</body>
</html>
