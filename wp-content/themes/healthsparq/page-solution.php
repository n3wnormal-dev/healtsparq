<?php
/*
Template Name: Solution
*/
get_header(); ?>

<div class="container">

	<?php

		$header_img = get_field('header_image');
		$uuid = uniqid('img');
		if ($header_img) {
			echo Picture::create( 'style', $header_img, array(
			    'selector' => '#'.$uuid
			) );
		}

		$siblings = wp_list_pages(array(
		  'child_of' => $post->post_parent,
		  'depth' => 1,
		  'echo' => false,
		  'title_li' => null
		));

	?>

	<header class="landing-header" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<div class="copy">
			<label class="type"><?php the_title(); ?></label>
			<?php the_field('header_content'); ?>
		</div>
		<?php if ($siblings && $post->post_parent != '') { ?>
			<nav class="subnav solutions-subnav">
				<ul class="header-subnav desktop" id="solutions-subnav">
					<li><a href="<?php the_permalink($post->post_parent); ?>">Our Solutions</a></li>
					<?php echo $siblings; ?>
				</ul>

				<div class="dropdown-container mobile">
					<div class="dropdown closed">
						<div class="title">
							<?php echo the_title(); ?>
						</div>
						<div class="dropdown-menu" style="height:0;">
							<ul>
								<li><a href="<?php the_permalink($post->post_parent); ?>">Our Solutions</a></li>
								<?php echo $siblings; ?>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		<?php } ?>
	</header>

	<section class="main solution">
		<?php the_content(); ?>
	</section>

	<?php $blocks = get_field('callouts'); ?>
	<?php if ($blocks) { ?>
	<section class="callouts">
	  <h2><?php the_field('section_heading'); ?></h2>
	  <?php
	    $i = 0;
	    $layouts = array();
	    foreach ($blocks as $block) {
	      include('flex-layouts/'.$block['acf_fc_layout'].'.php');
	      $layouts[] = $block['acf_fc_layout'];
	      $i++;
	    }
	    // print_r($layouts);
	  ?>
	  <div class="clear"></div>
	  <?php if ( in_array('video_header', $layouts)  || in_array('small_video', $layouts) ) { ?>
	    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/library/js/video.js"></script>
	  <?php } ?>
	</section>
	<?php } ?>


</div>

<?php get_footer(); ?>

</body>
</html>
