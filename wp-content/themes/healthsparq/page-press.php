<?php
/*
Template Name: Press
*/
get_header();

$args = array(
	'post_type' => 'press',
	// 'orderby' => 'date',
	'order' => 'DESC',
);

$args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

$posts = new WP_Query($args);

// Pagination fix
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $posts;

?>

<div class="container">

	<?php include('inc/landing-header.php'); ?>

	<section class="index press-index layoutPending">
		<div class="block-sizer"></div>
		<div class="gutter-sizer"></div>
		<?php while ($posts->have_posts()) : $posts->the_post(); ?>
			<?php include('inc/press-block.php'); ?>
		<?php wp_reset_postdata(); endwhile; ?>
		<div class="clear"></div>
	</section>

	<?php include('inc/pagination.php'); ?>

	<?php wp_reset_query(); ?>

</div>

<?php

// Reset main query object
$wp_query = NULL;
$wp_query = $temp_query;

?>

<?php get_footer(); ?>

</body>
</html>
