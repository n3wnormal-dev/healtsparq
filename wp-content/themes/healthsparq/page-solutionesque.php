<?php
/*
Template Name: Solutionsesque Landing
*/
get_header(); ?>

<div class="container ">

	<?php

		$header_img = get_field('header_image');
		$uuid = uniqid('img');
		if ($header_img) {
			echo Picture::create( 'style', $header_img, array(
			    'selector' => '#'.$uuid
			) );
		}


	?>

	<header class="landing-header" id="<?php echo $uuid; ?>">
		<div class="overlay"></div>
		<div class="copy">
			<label class="type"><?php the_title(); ?></label>
			<?php the_field('header_content'); ?>
		</div>
	</header>

	<?php $sections = get_field('content_blocks'); ?>
	<?php if ($sections) { ?>

		<?php
			$i = 0;
			foreach ($sections as $section) {
				include('flex-layouts/'.$section['acf_fc_layout'].'.php');
				$i++;
			}
		?>
		<!-- <div class="clear"></div> -->

	<?php } ?>

	<?php $blocks = get_field('callouts'); ?>
	<?php if ($blocks) { ?>
	<section class="callouts">
	  <h2><?php the_field('section_heading'); ?></h2>
	  <?php
	    $i = 0;
	    $layouts = array();
	    foreach ($blocks as $block) {
	      include('flex-layouts/'.$block['acf_fc_layout'].'.php');
	      $layouts[] = $block['acf_fc_layout'];
	      $i++;
	    }
	    // print_r($layouts);
	  ?>
	  <div class="clear"></div>
	  <?php if ( in_array('video_header', $layouts)  || in_array('small_video', $layouts) ) { ?>
	    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/library/js/video.js"></script>
	  <?php } ?>
	</section>
	<?php } ?>


</div>

<?php get_footer(); ?>

</body>
</html>
