(function($) {

$(document).ready(function(){

  // console.log(detectIE());

  var ua = window.navigator.userAgent;
  var is_ie = /MSIE|Trident/.test(ua);

  if ( is_ie ) {
    $('body').addClass('dIE');
  }

  /*----------- Homepage animations -----------*/

  var screenWidth = window.innerWidth;
  var screenHeight = window.innerHeight;
  var landscapeMode;
  var portraitMode;

  if ( screenWidth >= screenHeight ) {
    landscapeMode = true;
    portraitMode = false;
  } else {
    landscapeMode = false;
    portraitMode = true;
  }



  if ( screenWidth > 768 ) {
    $('.qa-slider').flexslider({
        pauseOnHover: false,
        slideshow: true,
        slideshowSpeed: 9000,
        smoothHeight: false,
        keyboard: false,
        animation: 'fade',
        directonNav: false,
        controlNav: false,
        animationLoop: true,
        start: function(){
          $('.qa-slide:first-of-type .question').typeIt({
            callback: function() {
              $('.hero > .copy').fadeOut();
              $('.qa-slide:first-of-type .answer').addClass('visible');
            }
          });
        },
        after: function(){
            var $flexActiveSlideQuestions = $('.flex-active-slide .question');
            $flexActiveSlideQuestions.typeIt({
            callback: function() {
              $('.flex-active-slide .answer').addClass('visible');
            },
              strings: [
                  $flexActiveSlideQuestions.data('question')
              ],
              breakLines: false,
          });
        }
    });
  }

  window.odometerOptions = {
    auto: false,
    selector: '.odometer',
    format: '(.ddd)',
    duration: 2500,
  };

  $(window).scroll(function(){
		if ($('body').is('.home')) {

      // Trigger Number Animation
      if ($(window).scrollTop() > $('section.numbers').offset().top - 400) {
  			$('.odometer').each(function(index){
          var count = $(this).data('value');
          $(this).text(count);
        });
  		}

      // Change Top Nav
      if ($(window).scrollTop() >= 750) {
  			$('header.top-bar').removeClass('special');
  		} else {
  			$('header.top-bar').addClass('special');
  		}
    }
	});

}); // End Document Ready

})( jQuery );
