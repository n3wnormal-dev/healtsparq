function is_touch_device() {
  return 'ontouchstart' in window // works on most browsers
  || 'onmsgesturechange' in window; // works on ie10
}

(function($) {

  function is_scrolled_in(elem) {
      var $elem = $(elem);
      var $window = $(window);

      var docViewTop = $window.scrollTop();
      var docViewBottom = docViewTop + $window.height();

      var elemTop = $elem.offset().top;
      var elemBottom = elemTop + $elem.height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }

  // Get IE or Edge browser version
  var version = detectIE();

  // if (version === false) {
  //   document.getElementById('result').innerHTML = '<s>IE/Edge</s>';
  // } else if (version >= 12) {
  //   document.getElementById('result').innerHTML = 'Edge ' + version;
  // } else {
  //   document.getElementById('result').innerHTML = 'IE ' + version;
  // }


  function detectIE() {

    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // IE 12 => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  }

$(document).ready(function(){

  // console.log('UA: '+version);

  var screenWidth = window.innerWidth;
  var screenHeight = window.innerHeight;
  var landscapeMode;
  var portraitMode;

  if ( screenWidth >= screenHeight ) {
    landscapeMode = true;
    portraitMode = false;
  } else {
    landscapeMode = false;
    portraitMode = true;
  }

  transformicons.add('.tcon');

	$('#hamburger').click(function(){
		$('body,header').toggleClass('open');
	});

	$('.main, footer').click(function(){
		$('body,header').removeClass('open');
		transformicons.revert('#hamburger');
	});

	$(document).keyup(function(e) {
	  	if (e.keyCode == 27) {
	  		$('body,header').removeClass('open');
			transformicons.revert('#hamburger');
	  	}
	});

  /* --- Some fancy parallaxing  --- */

	// $(window).scroll(function(){
  //
	// 	var y = $(window).scrollTop();
	// 	var h = $(window).height();
	// 	var t = y/2 + 'px';
	// 	var o = ((h - y*2) > 0) ? (h - y*2)/h : 0;
  //
	// 	$('section.hero .background, section.header .background').css('transform','translate3d(0,'+t+',0)');
	// 	$('section.hero .content, section.header .content').css('opacity',o);
  //
	// });

  // Filter Dropdown
  if ($('.dropdown-container')) {

    $('.dropdown-container').each(function(){
      var $container = $(this).find('.dropdown-menu'),
      $list = $(this).find('.dropdown-menu ul'),
      listItem = $list.find('li'),
      listLen = listItem.length,
      listHeight = 200;

    // $(function(){
    //   var current = $($list).find('li.current-cat a').text();
    //   $('.dropdown .title').text(current);
    // });
    if ( $('body').hasClass('category') ) {
      var current = $list.find('li.current-cat a').text();
      $('.dropdown .title').text(current);
    } else if ( $('body').hasClass('blog') ) {
      $list.find('li.cat-item-all').addClass('current-cat');
    }

    $(this).find(".dropdown .title").click(function () {
      // console.log('Container Height: '+$container.height());
      if( $container.height() != 0) {
        closeMenu(this);
      } else {
        openMenu(this);
      }
    });

    $(this).find(".dropdown-menu li a").click(function () {
      closeMenu(this);
    });

    function closeMenu(el) {
      $(el).closest('.dropdown').toggleClass("closed").find(".title > label").text($(el).text());
      $container.css("height", 0);
      $list.css( "top", 0 );
    }

    function openMenu(el) {
      // console.log('Open');
      $(el).parent().removeClass("closed");

      if (screenWidth >= 992) {
        // console.log('Big screen: '+screenWidth);
        if ( listLen < 6) {
          console.log('Short list of '+listLen+' items');
          listHeight = ((listLen - 1) * 40);
          $container.css({
            height: listHeight
          });
        } else {
          // console.log('Long list of '+listLen+' items');
          $container.css({
            height: listHeight
          })
          .mousemove(function(e) {
            var heightDiff = $list.height() / $container.height(),
                offset = $container.offset(),
                relativeY = (e.pageY - offset.top),
                top = relativeY*heightDiff > $list.height()-$container.height() ?
                      $list.height()-$container.height() : relativeY*heightDiff;

            $list.css("top", -top);
          });
        }
      } else {
        // console.log('Small screen: '+screenWidth);
        fulllistHeight = ((listLen - 1) * 40);
        $container.css({
          height: fulllistHeight
        });
      }
    }
    });

  }

  // Responsive Footer Accordian Menu
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight){
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    }
  }


  // var acc2 = document.getElementsByClassName("accordion");
  var acc2 = $('nav.mobile li.menu-item-has-children > a');
  var i;

  for (i = 0; i < acc2.length; i++) {
    acc2[i].onclick = function(e) {
      e.preventDefault();
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight){
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    }
  }


  // Functions for Search Form
  $('.remodal.search input:enabled:visible:first').focus();


  // GA Tagging for Event Tracking

  $('header .menu-item-719 > a').click(function(){
    gtag('event', 'demo_request_click', {
      'event_category': 'Demo Request Click',
      'event_label': 'Header'
    });
  });

  $('footer .super-footer a.btn').click(function(){
    gtag('event', 'demo_request_click', {
      'event_category': 'Demo Request Click',
      'event_label': 'Footer'
    });
  });

  $('li.mega-menu-item').on('open_panel', function() {
//       $(".container").css("filter", "blur(1.5px)");
	  $(".container").css("filter", "blur(2px) opacity(.2)");
	  $(".callouts").css("filter", "blur(2px)");
  });
	
	$('li.mega-menu-item').on('close_panel', function() {
		$(".container").css("filter", "blur(0px)");
// 		$(".container").css("filter", "blur(0px) opacity(1)");
		$(".callouts").css("filter", "blur(0px)");
	});

}); // End Document Ready


})( jQuery );
