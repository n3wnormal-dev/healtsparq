(function($) {

$(document).ready(function(){

  $('.team-slider').flexslider({
      pauseOnHover: false,
      slideshow: true,
      slideshowSpeed: 5000,
      smoothHeight: false,
      keyboard: false,
      animation: 'fade',
      // directionNav: false,
      customDirectionNav: $('.nav-arrows a'),
      // initDelay: 0,
      // start: function(slider) { // Fires when the slider loads the first slide
      //   var slide_count = slider.count - 1;
      //
      //   $(slider)
      //     .find('img.lazy:eq(0),img.lazy:eq(1)')
      //     .each(function() {
      //       var src = $(this).attr('data-src');
      //       $(this).attr('src', src).removeAttr('data-src').removeClass('lazy');
      //     });
      // },
      // before: function(slider) { // Fires asynchronously with each slider animation
      //   var slides     = slider.slides,
      //       index      = slider.animatingTo,
      //       $slide     = $(slides[index]),
      //       $img       = $slide.find('img[data-src]'),
      //       current    = index,
      //       nxt_slide  = current + 1,
      //       prev_slide = current - 1;
      //
      //   $slide
      //     .parent()
      //     .find('img.lazy:eq(' + current + '), img.lazy:eq(' + prev_slide + '), img.lazy:eq(' + nxt_slide + ')')
      //     .each(function() {
      //       var src = $(this).attr('data-src');
      //       $(this).attr('src', src).removeAttr('data-src').removeClass('lazy');
      //     });
      // }
      // init: function (slider) {
      //       // lazy load
      //       $("img.lazy").slice(0,5).each(function () {
      //           var src = $(this).attr("data-src");
      //           $(this).attr("src", src).removeAttr("data-src").removeClass("lazy");
      //       });
      //   },
      //   before: function (slider) {
      //       // lazy load
      //       $("img.lazy").slice(0,3).each(function () {
      //           var src = $(this).attr("data-src");
      //           $(this).attr("src", src).removeAttr("data-src").removeClass("lazy");
      //       });
      //   }
  });

  $('.team-slide .image').hover(
    function(){
      $(this).find('.pie').show();
      $(this).find('.no-pie').hide();
    },
    function(){
      $(this).find('.pie').hide();
      $(this).find('.no-pie').show();
    }
  );


  // Force Leadership block ot open single page in mobile, modal on desktop
  var screenWidth = window.innerWidth;
  var screenHeight = window.innerHeight;

  if ( screenWidth < 640 ) {
    $('a.leader').click(function(e){
      var target = $(this).data('href');
      e.preventDefault();
      window.location.href = target;
    });
  } else {
    $('a.leader').click(function(e){
      var target = $(this).data('remodal-target');
      $('[data-remodal-id='+target+']').remodal();
    });
  }


}); // End Document Ready

})( jQuery );
