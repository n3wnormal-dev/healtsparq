
/* Script for asynchronus load of sharing buttons */

 (function(w, d, s) {

   function go(){
  	var js, fjs = d.getElementsByTagName(s)[0], load = function(url, id) {
  	if (d.getElementById(id)) {return;}
    	js = d.createElement(s); js.src = url; js.id = id;
    	fjs.parentNode.insertBefore(js, fjs);
  	};
        //Facebook
  	load('//connect.facebook.net/en_US/all.js#xfbml=1', 'fbjssdk');
        //Google+
  	load('https://apis.google.com/js/plusone.js', 'gplus1js');
        //Twitter
  	load('//platform.twitter.com/widgets.js', 'tweetjs');
        //LinedIN
  	load('//platform.linkedin.com/in.js', 'lnkdjs');
        //Pinterest
  	load('//assets.pinterest.com/js/pinit.js', 'pinitjs');
    }
 	if (w.addEventListener) { w.addEventListener("load", go, false); }
  	else if (w.attachEvent) { w.attachEvent("onload",go); }

}(window, document, 'script'));
