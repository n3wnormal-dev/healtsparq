(function() {
  var v = document.getElementsByClassName("youtube-player");
  for (var n = 0; n < v.length; n++) {
    v[n].onclick = function () {
      console.log('Clicked: '+ this.id);
      var iframe = document.createElement("iframe"); iframe.setAttribute("src", "//www.youtube.com/embed/" + this.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&modestbranding=1&enablejsapi=1&rel="+ this.dataset.related +"&controls="+this.dataset.control+"&showinfo=" + this.dataset.info+"&origin"+this.dataset.origin);
      iframe.setAttribute('title',this.dataset.title);
      iframe.setAttribute('width',this.dataset.width);
      iframe.setAttribute('height',this.dataset.height);
      iframe.setAttribute("frameborder", "0");
      iframe.setAttribute("id", "youtube-iframe");
      // iframe.setAttribute("style", "width: 100%; height: 100%; position: absolute; top: 0; left: 0;");
      if (this.dataset.fullscreen == 1){
        iframe.setAttribute("allowfullscreen", "");
      }
      while (this.firstChild) {
        this.removeChild(this.firstChild);
      }
      this.appendChild(iframe);
    };
  }
})();
