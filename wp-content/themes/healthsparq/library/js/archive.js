(function($) {

$(document).ready(function(){

  // Index Grids
  var $grid = $('.index').masonry({
    // options
    itemSelector: '.block',
    columnWidth: '.block-sizer',
    gutter: '.gutter-sizer',
    percentPosition: true,
    initLayout: false, // disable initial layout
  });

  function onLayout() {
    // console.log('layout done');
    $('.index').removeClass('layoutPending').addClass('layoutComplete');
  }
  // bind event listener
  $grid.on( 'layoutComplete', onLayout );

  $grid.masonry();


  // Filter Dropdown
  if ($('.dropdown-container')) {

    $('.dropdown-container').each(function(){
      var $container = $(this).find('.dropdown-menu'),
      $list = $(this).find('.dropdown-menu ul'),
      listItem = $list.find('li'),
      listLen = listItem.length,
      listHeight = 200;

    // $(function(){
    //   var current = $($list).find('li.current-cat a').text();
    //   $('.dropdown .title').text(current);
    // });
    if ( $('body').hasClass('category') ) {
      var current = $list.find('li.current-cat a').text();
      $('.dropdown .title').text(current);
    } else if ( $('body').hasClass('blog') ) {
      $list.find('li.cat-item-all').addClass('current-cat');
    }

    $(this).find(".dropdown .title").click(function () {
      if( $container.height() > 0) {
        closeMenu(this);
      } else {
        openMenu(this);
      }
    });

    $(this).find(".dropdown-menu li a").click(function () {
      closeMenu(this);
    });

    function closeMenu(el) {
      $(el).closest('.dropdown').addClass("closed").find(".title > label").text($(el).text());
      $container.css("height", 0);
      $list.css( "top", 0 );
    }

    function openMenu(el) {
      $(el).parent().removeClass("closed");

      if ( listLen < 6) {
        listHeight = ((listLen - 1) * 40);
        $container.css({
          height: listHeight
        });
      } else {
        $container.css({
          height: listHeight
        })
        .mousemove(function(e) {
          var heightDiff = $list.height() / $container.height(),
              offset = $container.offset(),
              relativeY = (e.pageY - offset.top),
              top = relativeY*heightDiff > $list.height()-$container.height() ?
                    $list.height()-$container.height() : relativeY*heightDiff;

          $list.css("top", -top);
        });
      }
    }
    });

  }

}); // End Document Ready


})( jQuery );
