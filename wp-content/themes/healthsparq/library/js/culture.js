(function($) {

$(document).ready(function(){

  $('.life-slider').flexslider({
      pauseOnHover: false,
      slideshow: true,
      slideshowSpeed: 5000,
      smoothHeight: false,
      keyboard: false,
      animation: 'slide',
      directonNav: true,
      controlNav: false
  });

}); // End Document Ready

})( jQuery );
