<?php
/* Welcome to Bones :)
This is the core Bones file where most of the
main functions & features reside. If you have
any custom functions, it's best to put them
in the functions.php file.

Developed by: Eddie Machado
URL: http://themble.com/bones/

  - head cleanup (remove rsd, uri links, junk css, ect)
  - enqueueing scripts & styles
  - theme support functions
  - custom menu output & fallbacks
  - related post function
  - page-navi function
  - removing <p> from around images
  - customizing the post excerpt

*/

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function bones_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'bones_remove_wp_ver_css_js', 9999 );

} /* end bones head cleanup */



function add_async_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_async = array('transformicons-js', 'bones-modernizr', 'main-js', 'about-js', 'culture-js', 'archive-js');

   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', ' async="async" src', $tag);
      }
   }
   return $tag;
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


// A better title
// http://www.deluxeblogtips.com/2012/03/better-title-meta-tag.html
function rw_title( $title, $sep, $seplocation ) {
  global $page, $paged;

  // Don't affect in feeds.
  if ( is_feed() ) return $title;

  // Add the blog's name
  if ( 'right' == $seplocation ) {
    $title .= get_bloginfo( 'name' );
  } else {
    $title = get_bloginfo( 'name' ) . $title;
  }

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );

  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title .= " {$sep} {$site_description}";
  }

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 ) {
    $title .= " {$sep} " . sprintf( __( 'Page %s', 'dbt' ), max( $paged, $page ) );
  }

  return $title;

} // end better title

// remove WP version from RSS
function bones_rss_version() { return ''; }

// remove WP version from scripts
function bones_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// remove injected CSS for recent comments widget
function bones_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function bones_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

// remove injected CSS from gallery
function bones_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}


/*********************
SCRIPTS & ENQUEUEING
*********************/


// loading modernizr and jquery, and reply script

//Making jQuery to load from Google Library
function replace_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery-cdn', 'https://code.jquery.com/jquery-3.3.1.min.js', null, '3.1.1', false);
		wp_enqueue_script('jquery-cdn');


	}
}

function bones_scripts_and_styles() {

  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

	// $version = '1.2'; // prod
	$version = wp_rand( 1, 100 ); // dev only
	// $version = null; // dev only

  if (!is_admin()) {

		// modernizr (without media query polyfill)
		wp_register_script( 'bones-modernizr', get_stylesheet_directory_uri() . '/library/js/vendor/modernizr.custom.min.js', array(), '2.5.3', false );


	// --------------------------
	// Third-Party CSS
	// --------------------------
		// wp_register_style( 'transformicons', get_stylesheet_directory_uri() . '/library/css/vendor/transformicons.css', array(), '', 'all' );

		// register remodal stylesheet
		// wp_register_style( 'remodal', get_stylesheet_directory_uri() . '/library/css/vendor/remodal.css', array(), '', 'all' );

		// register remodal theme stylesheet
		// wp_register_style( 'remodal-theme', get_stylesheet_directory_uri() . '/library/css/vendor/remodal-default-theme.css', array(), '', 'all' );

		// odometer for homepage numbers animation
		// wp_register_style( 'odometer-theme', get_stylesheet_directory_uri() . '/library/css/vendor/odometer-theme-minimal.css', array(), '', 'all' );

		// odometer for homepage numbers animation
		// wp_register_style( 'flexslider', get_stylesheet_directory_uri() . '/library/css/vendor/flexslider.css', array(), '', 'all' );



		// --------------------------
		// Theme CSS
		// --------------------------

		// register critical stylesheet
		wp_register_style( 'critical-styles', get_stylesheet_directory_uri() . '/library/css/critical.css', array(), $version, 'all' );

		// register main stylesheet
		wp_register_style( 'main-styles', get_stylesheet_directory_uri() . '/library/css/main.css', array(), $version, 'all' );

		// ie-only style sheet
		wp_register_style( 'bones-ie-only', get_stylesheet_directory_uri() . '/library/css/ie.css', array(), $version );

    // comment reply script for threaded comments
    // if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
		//   wp_enqueue_script( 'comment-reply' );
    // }

		// Page or Content-Specific Styles
		wp_register_style( 'single-styles', get_stylesheet_directory_uri() . '/library/css/for/single.css', array(), $version, 'all' );

		// wp_register_style( 'archive-styles', get_stylesheet_directory_uri() . '/library/css/for/archive.css', array(), $version, 'all' );
		wp_register_style( 'blog-styles', get_stylesheet_directory_uri() . '/library/css/for/blog.css', array(), $version, 'all' );

		wp_register_style( 'events-styles', get_stylesheet_directory_uri() . '/library/css/for/events.css', array(), $version, 'all' );
		wp_register_style( 'press-styles', get_stylesheet_directory_uri() . '/library/css/for/press.css', array(), $version, 'all' );
		wp_register_style( 'resources-styles', get_stylesheet_directory_uri() . '/library/css/for/resources.css', array(), $version, 'all' );

		wp_register_style( 'culture-styles', get_stylesheet_directory_uri() . '/library/css/for/culture.css', array(), $version, 'all' );
		// wp_register_style( 'culture-tiny-styles', get_stylesheet_directory_uri() . '/library/css/for/culture_tiny.css', array(), $version, 'screen and (max-width: 640px)' );

		wp_register_style( 'solutions-styles', get_stylesheet_directory_uri() . '/library/css/for/solutions.css', array(), $version, 'all' );

		wp_register_style( 'about-styles', get_stylesheet_directory_uri() . '/library/css/for/about.css', array(), $version, 'all' );

		wp_register_style( 'home-styles', get_stylesheet_directory_uri() . '/library/css/for/home.css', array(), $version, 'all' );
		// wp_register_style( 'home-medium-styles', get_stylesheet_directory_uri() . '/library/css/for/home_medium.css', array(), $version, '(max-width: 991px)' );
		// wp_register_style( 'home-small-styles', get_stylesheet_directory_uri() . '/library/css/for/home_small.css', array(), $version, '(max-width: 768px)' );
		// wp_register_style( 'home-tiny-styles', get_stylesheet_directory_uri() . '/library/css/for/home_tiny.css', array(), $version, '(max-width: 640px)' );

		wp_register_style( 'search-styles', get_stylesheet_directory_uri() . '/library/css/for/search.css', array(), $version, 'all' );

		wp_register_style( '404-styles', get_stylesheet_directory_uri() . '/library/css/for/404.css', array(), $version, 'all' );

		wp_register_style( 'custom', get_stylesheet_directory_uri() . '/library/css/custom.css', array(), $version, 'all' );


		// --------------------------
		// Third-Party JS
		// --------------------------

		// jquery
		// wp_register_script( 'jquery-cdn', 'https://code.jquery.com/jquery-3.3.1.min.js', array(), '', false );

		// transformicons for hamburger
		wp_register_script( 'transformicons-js', get_stylesheet_directory_uri() . '/library/js/vendor/transformicons.min.js', array( 'jquery-cdn' ), true );

		// plyr script for videos
		// wp_register_script( 'plyr-js', get_stylesheet_directory_uri() . '/library/js/vendor/plyr.js', array( 'jquery' ), '', true );

		// remodal for modals
		wp_register_script( 'remodal-js', get_stylesheet_directory_uri() . '/library/js/vendor/remodal.min.js', array( 'jquery-cdn' ), '', true );

		// flexslider for slides
		wp_register_script( 'flexslider-js', get_stylesheet_directory_uri() . '/library/js/vendor/jquery.flexslider-min.js', array( 'jquery-cdn' ), '', true );

		// masonry for index grids
		wp_register_script( 'masonry-js', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', array( 'jquery-cdn' ), '', true );

		// marketo forms
		wp_register_script( 'marketo-js', '//app-ab09.marketo.com/js/forms2/js/forms2.min.js', array( 'jquery-cdn' ), '', false );

		// odometer for homepage numbers animation
		wp_register_script( 'odometer-js', get_stylesheet_directory_uri() . '/library/js/vendor/odometer.min.js', array( 'jquery-cdn' ), '', true );

		// typeit for homepage Q&A animation
		wp_register_script( 'typeit-js', '//cdnjs.cloudflare.com/ajax/libs/typeit/4.4.0/typeit.min.js', array( 'jquery-cdn' ), '', true );





		// --------------------------
		// Theme JS
		// --------------------------

		//adding scripts file in the footer
		wp_register_script( 'main-js', get_stylesheet_directory_uri() . '/library/js/main.js', array( 'jquery-cdn' ), $version, true );

		wp_register_script( 'about-js', get_stylesheet_directory_uri() . '/library/js/about.js', array( 'jquery-cdn', 'flexslider-js' ), $version, true );

		wp_register_script( 'home-js', get_stylesheet_directory_uri() . '/library/js/home.js', array( 'jquery-cdn', 'flexslider-js', 'odometer-js', 'typeit-js' ), $version, true );

		wp_register_script( 'culture-js', get_stylesheet_directory_uri() . '/library/js/culture.js', array( 'jquery-cdn', 'flexslider-js' ), $version, true );

		wp_register_script( 'archive-js', get_stylesheet_directory_uri() . '/library/js/archive.js', array( 'jquery-cdn', 'masonry-js' ), $version, true );

		wp_register_script( 'single-js', get_stylesheet_directory_uri() . '/library/js/single.js', array( 'jquery-cdn' ), $version, true );




		// enqueue styles
		// wp_enqueue_style( 'transformicons' );
		// wp_enqueue_style( 'remodal' );
		// wp_enqueue_style( 'remodal-theme' );
		// wp_enqueue_style( 'critical-styles' );
    wp_enqueue_style( 'critical-styles' );
    wp_enqueue_style( 'custom' );
		wp_enqueue_style( 'bones-ie-only' );

		$wp_styles->add_data( 'bones-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

		// enqueue scripts
		wp_enqueue_script( 'bones-modernizr' );
		/*
		I recommend using a plugin to call jQuery
		using the google cdn. That way it stays cached
		and your site will load faster.
		*/
		// wp_enqueue_script( 'jquery-cdn' );
		wp_enqueue_script( 'transformicons-js' );
		wp_enqueue_script( 'remodal-js' );
		wp_enqueue_script( 'main-js' );

    // Enqueue JS
    if (is_singular()) {

			global $wp_query;
			//Check which template is assigned to current page we are looking at
			$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );

			if (!is_front_page() && !is_home()) {
				// wp_enqueue_style( 'single-styles' );
				wp_enqueue_script( 'single-js' );
			}

			// if ($post_types = 'resources' || $template_name == 'page-form.php') {
			// 	wp_enqueue_script( 'marketo-js' );
			// }

			if (is_page()) {

				// if ($template_name == 'page-solutionesque.php' || $template_name == 'page-solution.php' || $template_name == 'page-solutions.php') {
				// 	wp_enqueue_style( 'solutions-styles' );
				// }

				if ($page = '15' || $page = '33' || $page = '71' || $template_name == 'page-about.php' || $template_name == 'page-life-and-careers.php') {
					// wp_enqueue_style( 'flexslider' );
					wp_enqueue_script( 'flexslider-js' );
				}
				if (is_page('15') || is_front_page() && is_home()) {
					// wp_enqueue_style( 'odometer-theme' );
					// wp_enqueue_style( 'home-styles' );
					wp_enqueue_script( 'typeit-js' );
					wp_enqueue_script( 'odometer-js' );
					wp_enqueue_script( 'home-js' );
				}
				if (is_page('33') || $template_name == 'page-about.php') {
					// wp_enqueue_style( 'about-styles' );
					wp_enqueue_script( 'about-js' );
				}
				if (is_page('71') || $template_name == 'page-life-and-careers.php') {
					// wp_enqueue_style( 'culture-styles' );
					wp_enqueue_script( 'culture-js' );
				}
				if ($page = '75') {
					// wp_enqueue_style( 'events-styles' );
				}
			} // END is_page()



		} // END is_singular()
    if (!is_front_page() && is_home() || is_category()) {
			// wp_enqueue_style( 'blog-styles' );
			wp_enqueue_script( 'masonry-js' );
			wp_enqueue_script( 'archive-js' );
		}

		if (is_archive() || is_tax()) {
			// wp_enqueue_style( 'archive-styles' );
			wp_enqueue_script( 'masonry-js' );
			wp_enqueue_script( 'archive-js' );
		}

		if (( is_tax('resource_type') || is_post_type_archive('resources') || is_singular('resources'))) {
			// wp_enqueue_style( 'resources-styles' );
		}

		if (is_post_type_archive('press')) {
			// wp_enqueue_style( 'press-styles' );
			wp_enqueue_script( 'masonry-js' );
			wp_enqueue_script( 'archive-js' );
		}

		if (is_404()) {
			// wp_enqueue_style( '404-styles' );
		}

		if (is_search()) {
			// wp_enqueue_style( 'search-styles' );
		}


		wp_enqueue_style( 'main-styles' );

		// if (is_singular()) {

			global $wp_query;
			//Check which template is assigned to current page we are looking at
			$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );

			if (!is_front_page() && !is_home() || is_single() || is_singular('resources')) {
				wp_enqueue_style( 'single-styles' );
				// wp_enqueue_script( 'single-js' );
			}

			// if (is_page()) {

				if ($template_name == 'page-solutionesque.php' || $template_name == 'page-solution.php' || $template_name == 'page-solutions.php') {
					wp_enqueue_style( 'solutions-styles' );
				}

				if (is_page('15')) {
					// wp_enqueue_style( 'odometer-theme' );
					wp_enqueue_style( 'home-styles' );
					// wp_enqueue_style( 'home-medium-styles' );
					// wp_enqueue_style( 'home-small-styles' );
					// wp_enqueue_style( 'home-tiny-styles' );
				}
				if (is_page('33') || $template_name == 'page-about.php') {
					wp_enqueue_style( 'about-styles' );
				}
				if (is_page('71') || $template_name == 'page-life-and-careers.php') {
					wp_enqueue_style( 'culture-styles' );
				}
				if (is_page('75')) {
					wp_enqueue_style( 'events-styles' );
				}
			// } // END is_page()



		// } // END is_singular()

		if (!is_front_page() && is_home() || is_category()) {
			wp_enqueue_style( 'blog-styles' );
		}

		if (is_archive() || is_tax()) {
			// wp_enqueue_style( 'archive-styles' );
		}

		if (( is_tax('resource_type') || is_post_type_archive('resources') || is_singular('resources'))) {
			wp_enqueue_style( 'resources-styles' );
		}

		if (is_post_type_archive('press')) {
			wp_enqueue_style( 'press-styles' );
		}

		if (is_404()) {
			wp_enqueue_style( '404-styles' );
		}

		if (is_search()) {
			wp_enqueue_style( 'search-styles' );
		}

	} // END !is_admin()
}

function bones_styles_in_footer() {
  if (!is_admin()) {


  } // END !IS_ADMIN()
}


/*********************
THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function bones_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// default thumb size
	set_post_thumbnail_size(300, 300, true);

	// rss thingy
	add_theme_support('automatic-feed-links');

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// adding post format support
	add_theme_support( 'post-formats',
		array(
			// 'aside',             // title less blurb
			// 'gallery',           // gallery of images
			'link',              // quick link to other site
			// 'image',             // an image
			// 'quote',             // a quick quote
			// 'status',            // a Facebook like status update
			// 'video',             // video
			// 'audio',             // audio
			// 'chat'               // chat transcript
		)
	);

	// wp menus
	add_theme_support( 'menus' );

	// registering wp3+ menus
	register_nav_menus(
		array(
			// 'main-nav' => __( 'The Main Menu', 'bonestheme' ),   // main nav in header
			// 'footer-links' => __( 'Footer Links', 'bonestheme' ) // secondary nav in footer
			'main-nav' => 'Main Nav',
			'footer-menu-1' => 'Footer Menu 1',
			'footer-menu-2' => 'Footer Menu 2',
			'footer-menu-3' => 'Footer Menu 3',
			'utility-menu' => 'Utility Menu'
		)
	);

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form'
	) );

} /* end bones theme support */

// Custom Permalinks for Resource Types
function wpa_resource_permalinks( $post_link, $post ){
  if ( is_object( $post ) && $post->post_type == 'resources' ){
      $terms = wp_get_object_terms( $post->ID, 'resource_type' );
      if( $terms ){
          return str_replace( '%resource_type%' , $terms[0]->slug , $post_link );
      }
  }
  return $post_link;
}
add_filter( 'post_type_link', 'wpa_resource_permalinks', 1, 2 );


/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using bones_related_posts(); )
function bones_related_posts() {
	global $post;
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) {
			$tag_arr .= $tag->slug . ',';
		}
		$args = array(
			'tag' => $tag_arr,
			'numberposts' => 3, /* you can change this to show more */
			'post__not_in' => array($post->ID)
		);
		} else {
			$args = array(
				'numberposts' => 3, /* you can change this to show more */
				'post__not_in' => array($post->ID)
			);
		}
		$related_posts = get_posts( $args );
		if($related_posts) {
			echo '<section class="related-container wrap cf">';
			echo '<h6>' . get_field('more_posts_heading','options') . '</h6>';
			foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
				<div class="related_post m-all t-1of3 d-1of3">
					<a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
					<?php if (has_post_thumbnail()) { the_post_thumbnail(); } else { ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/nothumb.jpg" />
						<?php } ?>
					<h5><?php the_title(); ?></h5>
					<p class="author small">
						by <?php the_author(); ?> <img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/icons/clock-blue.svg" />
						<?php echo get_the_date('F j'); ?>
					</p>
					</a>
				</div>
			<?php endforeach;
			echo '</section>';
		}
	wp_reset_postdata();

} /* end bones related posts function */

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function bones_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function bones_excerpt_more($more) {
	global $post;
	// edit here if you like
	return ' ...';
	//return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', 'bonestheme' ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &gt;', 'bonestheme' ) .'</a>';
}


/* --- Custom Helper Functions --- */

// function custom_excerpt($limit = 30) {
//     return wp_trim_words(get_the_content(), $limit, ' ... ');
// }

function short_date() {
  return date('m.d.Y', strtotime(get_the_date()));
}

function custom_excerpt_length($length) {
    return 20;
}

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return ' ... ';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function custom_read_more() {
    return ' ... ';
}

function custom_excerpt($limit = 30) {
    return wp_trim_words(get_the_content(), $limit, ' ... ');
}

function resource_excerpt($limit = 26) {
    return wp_trim_words(get_the_excerpt(), $limit, ' ... ');
}

function blog_excerpt($limit = 50) {
    return wp_trim_words(get_the_excerpt(), $limit, ' ... ');
}

function leader_excerpt($limit = 16) {
    return wp_trim_words(get_the_excerpt(), $limit, ' ... ');
}

function _get_excerpt($limit = 40) {
    return has_excerpt() ? get_the_excerpt() : wp_trim_words(strip_shortcodes(get_the_content()),$limit, '...');
}

// Browser Detection
// http://luke-danielson.com/2014/07/09/browser-detection-in-wordpress/
function user_client_detection_body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

	if($is_lynx)        $classes[] = 'lynx';
	else if($is_gecko)   $classes[] = 'gecko';
	else if($is_opera)   $classes[] = 'opera';
	else if($is_NS4)     $classes[] = 'ns4';
	else if($is_safari)  $classes[] = 'safari';
	else if($is_chrome)  $classes[] = 'chrome';
	else if($is_IE)      $classes[] = 'ie';
	else                $classes[] = 'unknown';

	if($is_IE &&
	   preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
	{
		$classes[] = 'ie'.$browser_version[1];
	}

	if(wp_is_mobile()) $classes[] = 'mobile';

	if($is_iphone) $classes[] = 'iphone';
	else if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) $classes[] = 'ipad';
	else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false) $classes[] = 'android';
	else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false) $classes[] = 'kindle';
	else if(strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false) $classes[] = 'blackberry';
	else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false) $classes[] = 'opera-mini';
	else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false) $classes[] = 'opera-mobi';

	if ( stristr( $_SERVER['HTTP_USER_AGENT'],'mac') ) $classes[] = 'osx';
	else if ( stristr( $_SERVER['HTTP_USER_AGENT'],'linux') ) $classes[] = 'linux';
	else if ( stristr( $_SERVER['HTTP_USER_AGENT'],'windows') ) $classes[] = 'windows';


	return $classes;
}

add_filter('body_class','user_client_detection_body_class');


/**
 * Count number of any given post type in the term
 *
 * @param string $taxonomy
 * @param string $term
 * @param string $postType
 * @return int
 */
function count_posts_in_term($taxonomy, $term, $postType = 'post') {
    $query = new WP_Query([
        'posts_per_page' => 1,
        'post_type' => $postType,
        'tax_query' => [
            [
                'taxonomy' => $taxonomy,
                'terms' => $term,
                'field' => 'slug'
            ]
        ]
    ]);

    return $query->found_posts;
}

// ACF Google max_num_pages
// function my_acf_init() {
//
// 	acf_update_setting('google_api_key', 'AIzaSyDRXKQAnPrSXJ80Je3pGg8Y5AL45c8OG50');
// }
//
// add_action('acf/init', 'my_acf_init');


?>
