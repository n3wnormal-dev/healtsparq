<?php
/* Bones Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function custom_post_types() {

	register_post_type( 'team',
		array(
			'labels' => array(
				'name' => __( 'Team' )
			),
			'menu_position' => 5,
			'menu_icon' => 'dashicons-groups',
			'public' => true,
			'has_archive' => false,
			'taxonomies' => array('post_tag'),
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'revisions',
				'excerpt'
			)
		)
	);

	register_post_type( 'events',
		array(
			'labels' => array(
				'name' => __( 'Events' )
			),
			'menu_position' => 5,
			'menu_icon' => 'dashicons-calendar',
			'public' => true,
			'has_archive' => false,
			'rewrite' => array( 'slug' => 'events', 'with_front' => false ),
			'taxonomies' => array('category','post_tag'),
			'supports' => array(
				'title',
				'editor',
				'revisions',
				'thumbnail',
				'post-formats'
			)
		)
	);

	register_post_type( 'press',
		array(
			'labels' => array(
				'name' => __( 'Press' )
			),
			'menu_position' => 5,
			'menu_icon' => 'dashicons-testimonial',
			'public' => true,
			'has_archive' => true,
			'rewrite' => array( 'slug' => 'press', 'with_front' => false ),
			'taxonomies' => array('category','post_tag'),
			'supports' => array(
				'title',
				'editor',
				'revisions',
				'thumbnail',
				'post-formats',
				'excerpt'
			)
		)
	);

	register_post_type( 'resources',
		array(
			'labels' => array(
				'name' => __( 'Resources' )
			),
			'menu_position' => 5,
			'menu_icon' => 'dashicons-lightbulb',
			'public' => true,
			'rewrite' => array( 'slug' => 'resources/%resource_type%', 'with_front' => false ),
			'has_archive' => 'resources',
			'taxonomies' => array('category','post_tag'),
			'supports' => array(
				'title',
				'editor',
				'revisions',
				'excerpt',
				'thumbnail'
			)
		)
	);

	remove_post_type_support( 'post', 'post-formats' );

	/* this adds your custom taxonomy to your custom post type */
	register_taxonomy_for_object_type( 'resource_type', 'resources' );
	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type( 'category', 'social' );
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type( 'post_tag', 'social' );

}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_types');


	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/

	// now let's add custom categories (these act like categories)
	register_taxonomy( 'resource_type',
		array('resources'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Resource Types', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Resource Type', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Resource Types', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Resource Types', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Resource Type', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Resource Type:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Resource Type', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Resource Type', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Resource Type', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Resource Type Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'resources', 'with_front' => false ),
		)
	);

	// now let's add custom tags (these act like categories)
	// register_taxonomy( 'custom_tag',
	// 	array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	// 	array('hierarchical' => false,    /* if this is false, it acts like tags */
	// 		'labels' => array(
	// 			'name' => __( 'Custom Tags', 'bonestheme' ), /* name of the custom taxonomy */
	// 			'singular_name' => __( 'Custom Tag', 'bonestheme' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Custom Tags', 'bonestheme' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Custom Tags', 'bonestheme' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Custom Tag', 'bonestheme' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Custom Tag:', 'bonestheme' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Custom Tag', 'bonestheme' ), /* edit custom taxonomy title */
	// 			'update_item' => __( 'Update Custom Tag', 'bonestheme' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Custom Tag', 'bonestheme' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Custom Tag Name', 'bonestheme' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true,
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 	)
	// );

	// function create_resource_types() {
	// 	register_taxonomy(
	// 		'resource_type',
	// 		'resources',
	// 		array(
	// 			'label' => __( 'Resource Types' ),
	// 			'rewrite' => array( 'slug' => 'resources', 'with_front' => false ),
	// 			'hierarchical' => true,
	// 			'show_admin_column' => true,
	// 		)
	// 	);
	// }
	// add_action( 'init', 'create_resource_types' );

	/*
		looking for custom meta boxes?
		check out this fantastic tool:
		https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
	*/


?>
