<?php
/*
Template Name: Life and Careers
*/
get_header(); ?>

<div class="container">

<?php include('inc/landing-header.php'); ?>

<section class="careers-vision">
 <div class="copy">
   <?php //the_field('careers_vision'); ?>
 </div>
 <div class="clear"></div>
</section>

<?php $slides = get_field('slideshow'); ?>

<section class="slideshow">
 <div class="flexslider life-slider">
   <ul class="slides">
     <?php	foreach ($slides as $slide) { ?>
       <li class="slide">
         <?php echo wp_get_attachment_image($slide['slide'],'large'); ?>
       </li>
     <?php } ?>
   </ul>
 </div>
 <div class="copy">
   <div class="outer-table">
     <div class="inner-table">
       <?php the_field('slideshow_copy'); ?>
     </div>
   </div>
 </div>
 <div class="clear"></div>
</section>

<?php $values = get_field('core_values'); ?>

<section class="core-values">
 <h2><?php the_field('core_values_header'); ?></h2>
 <div class="icons">
   <?php foreach ($values as $value) { ?>
     <div class="icon">
       <div class="image" style="background-image:url(<?php echo $value['icon']; ?>);"></div>
       <label><?php echo $value['label']; ?></label>
     </div>
   <?php } ?>
 </div>
 <div class="descriptions">
   <?php foreach ($values as $value) { ?>
     <div><?php echo $value['description']; ?></div>
   <?php } ?>
 </div>
</section>

<section class="careers">
 <h2><?php the_field('careers_header'); ?></h2>
 <?php the_field('careers_intro'); ?>
 <div class="grid">
   <div class="col-1-2">
     <?php the_field('careers_col_1'); ?>
   </div>
   <div class="col-1-2">
     <?php the_field('careers_col_2'); ?>
   </div>
 </div>
 <?php the_field('careers_link'); ?>
</section>

</div>

<?php get_footer(); ?>

</body>
</html>
