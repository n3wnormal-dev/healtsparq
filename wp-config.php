<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
define( 'DB_NAME', 'healthsparq' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8' );

define( 'WP_HOME', 'http://healthsparq.test' );

/** Database Charset to use in creating database tables. */

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

// define( 'WP_SITEURL', '//healthsparq.test' );
// define( 'WP_HOME',    '//healthsparq.test' );
define( 'FORCE_SSL', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.~YP[NBpWn]b`|Cv.t{6#[0l-|>)R_X|>%4j^>6{R;=gZN9?$K]nLQb<-j8Ek6>-');
define('SECURE_AUTH_KEY',  '{T_(|n=+Q;,Vd>([HQP5>XsP+Ggn#+l8pj)tWPa-9AOq%WQ1=<Nf{|r,7m|=G4R<');
define('LOGGED_IN_KEY',    'Bqm%,5-|-$/&UVVA_=WB.|Z{KVpp%pGh@5hP@NpgwBG;sC|-Ih=]U;gjfZ6Uy*;8');
define('NONCE_KEY',        'FOV-{<VT0c3:bf+T^H?JRO7<FrK])O7M;$U)5M{0 d*7pegBxk>e9|+B<Lm]|Smk');
define('AUTH_SALT',        'PdI4^B+q2D N/w+?fsCO)b8o&d@R*|<vO.pSs` U]AUyHfHhdG-1LM]9u:u!jBZX');
define('SECURE_AUTH_SALT', 'MA3,-O1^#?>k$:{)dOVHKSC9@>u*$JNLg2-Leg6[]|,na9BG.FbO[6Um3?L[3Gn2');
define('LOGGED_IN_SALT',   '`+EL--4D-_7vW%}t<y^*$|q_+(,(ahl-l=cE`oeggTcGH#$nvz9! D5+g&1y/CUP');
define('NONCE_SALT',       'U;(+~}uj.qOu+)l}4ny3(Mt6u2:yI@odsqn>p|Z9sJ9`fXXd6xX#o+^/p,/meC-2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

define( 'COOKIE_DOMAIN', false );
// define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST'] );

define('ALLOW_UNFILTERED_UPLOADS', true);

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
